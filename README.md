[![Build Status](https://travis-ci.org/OpenWakfu/wakfutcp.svg?branch=master)](https://travis-ci.org/OpenWakfu/wakfutcp)

# Synopsis
The goal of this project is to provide a set of combinators to quickly build Wakfu
clients and servers. By default it contains a set of messages and routines to perform
authentication and some other basic tasks. It also consists of a solid serialization framework that
supports a very large part of the game protocol. It uses the actor pattern and integrates with the Akka framework.

# Code example
Market reading actor:
```scala
  def receive: Receive = {
    case ServerList(servers, _) ⇒
      // this has to be sent to the sender because it's a different actor (authentication handler)
      val Some(srv) = servers.find(_.name == "Nox")
      sender() ! ServerChoice(srv)
    case CharactersListMessage(characters) ⇒
      import CharacterSerialized._
      val Some(char) =
        characters
          .map(_.members)
          .collectFirst {
            case c if c.select[Name].name == "Derp" => c
          }
      session !! CharacterSelectionMessage(char.select[Id].id, char.select[Name].name)
    case CharacterInformationMessage(_, ForLocalCharacterInformationSet(info), _, _) ⇒
      val pos = info.select[CharacterSerialized.Position]
      log.info(
        s"Logged character: ${info.select[CharacterSerialized.Name].name} Position(${pos.x}, ${pos.y}, ${pos.z})")
      /*
          the !! operator allows you to send any
          kind of message that you provide an
          implicit instance of OutboundMessageEncoder for
       */
      session !! InteractiveElementActionMessage(12224, 12)
    case MarketConsultResultMessage(sales, count) ⇒
      session !!
        MarketConsultRequestMessage(
          -1,
          -1,
          -1,
          -1,
          10.toShort,
          lowestMode = false,
          Array.empty
        )
      become(receiveAndAsk(sales.toList, 10))
  }

  /*
      market entries come by 10,
      here I gather them recursively
   */
  def receiveAndAsk(entries: List[MarketEntry], start: Int): Receive = {
    case MarketConsultResultMessage(sales, count) ⇒
      val next = start + 10
      if (next >= count) {
        handleData(entries ::: sales.toList)
      } else {
        session !!
          MarketConsultRequestMessage(
            -1,
            -1,
            -1,
            -1,
            next.toShort,
            lowestMode = false,
            Array.empty
          )
        become(receiveAndAsk(entries ::: sales.toList, next))
      }
  }

  def handleData(data: List[MarketEntry]): Unit = {
    // do stuff with data and quit

    session ! LogOut
    val _ = system.terminate()
  }
```

# Running
To use it you need sbt and scala compiler.

The easiest way to check it out is to clone this repository,
provide the necessary configuration in `examples/client/src/main/resources/application.conf`
and run `sbt clientExample/run`. Alternatively, if you want to run the server example, go to
`examples/server/src/main/resources` and run the `keygen.sh` script to generate keys and start the
server with `sbt serverExample/run`. The example server binds to the 8080 port.
