package wakfutcp.protocol.raw
import wakfutcp.protocol.generic._
import wakfutcp.protocol.raw.virtual.RawSpecificRooms

trait RawData

final case class RawNationGovernmentData(
  speech: String,
  governor: Option[RawNationGovernmentDataGovernor]
) extends RawData

final case class RawNationLaw(
  firstChange: Boolean,
  laws: Array[RawNationLawLaw]
) extends RawData

final case class RawNationDiplomacy(
  alignments: Array[RawNationDiplomacyAlignment],
  alignmentRequests: Array[RawNationDiplomacyRequest]
) extends RawData

final case class RawNationEconomy(
  spentCash: Array[RawNationEconomySpending],
  accumulatedTaxes: Array[RawNationEconomyProfit],
  totalCash: Option[RawNationEconomyTotalCash]
) extends RawData

final case class RawNationSurvey(
  governmentOpinions: Array[RawNationSurveyOpinion]
) extends RawData

final case class RawGuildStorage(
  compartments: Array[RawGuildStorageCompartment],
  money: Int,
  history: RawGuildStorageHistory
) extends RawData

final case class RawGuildStorageCompartment(
  id: Int,
  enabled: Boolean,
  items: Array[RawGuildStorageCompartmentItem],
  building: Option[RawGuildStorageCompartmentBuilding]
) extends RawData

final case class RawGuildStorageHistory(
  itemHistory: Array[RawGuildStorageHistoryHistoryItemEntry],
  moneyHistory: Array[RawGuildStorageHistoryHistoryMoneyEntry]
) extends RawData

final case class RawVault(
  items: Array[RawVaultItem],
  money: Int
) extends RawData

final case class RawInventoryHandler(
  questInventory: RawQuestItemInventory,
  temporaryInventory: RawInventoryItemInventory,
  cosmeticsInventory: RawCosmeticsItemInventory,
  petCosmeticsInventory: RawCosmeticsItemInventory
) extends RawData

final case class RawCosmeticsInventoryHandler(
  activeCosmetic: Option[RawCosmeticsInventoryHandlerActiveCosmetic]
) extends RawData

final case class RawLocks(
  locks: Array[RawLocksLock]
) extends RawData

final case class RawQuestItemInventory(
  items: Array[RawQuestItemInventoryRawQuestItem]
) extends RawData

final case class RawCosmeticsItemInventory(
  items: Array[RawCosmeticsItemInventoryRawCosmeticsItem],
  activeRefId: Int
) extends RawData

final case class RawInventoryItem(
  uniqueId: Long,
  refId: Int,
  quantity: Short,
  pet: Option[RawInventoryItemPet],
  xp: Option[RawInventoryItemXp],
  gems: Option[RawInventoryItemGems],
  rentInfo: Option[RawInventoryItemRentInfo],
  companionInfo: Option[RawInventoryItemCompanionInfo],
  bind: Option[RawInventoryItemBind],
  elements: Option[RawInventoryItemElements],
  mergedItems: Option[RawInventoryItemMergedItems],
  mimiSymbic: Option[RawInventoryItemMimiSymbic]
) extends RawData

final case class RawItemOptionalInfo(
  pet: Option[RawItemOptionalInfoPet],
  xp: Option[RawItemOptionalInfoXp],
  gems: Option[RawItemOptionalInfoGems],
  companion: Option[RawItemOptionalInfoCompanion]
) extends RawData

final case class RawItemXp(
  definitionId: Int,
  xp: Long
) extends RawData

final case class RawPet(
  definitionId: Int,
  name: String,
  colorItemRefId: Int,
  equippedRefItemId: Int,
  health: Int,
  xp: Int,
  lastMealDate: Long,
  lastHungryDate: Long,
  sleepRefItemId: Int,
  sleepDate: Long
) extends RawData

final case class RawGems(
  gems: Array[RawGemsContent],
  additionalGems: Array[RawGemsContent]
) extends RawData

final case class RawRentInfo(
  `type`: Int,
  duration: Long,
  count: Long
) extends RawData

final case class RawItemBind(
  `type`: Byte,
  applied: Boolean
) extends RawData

final case class RawItemElements(
  damageElements: Byte,
  resistanceElements: Byte
) extends RawData

final case class RawCompanionInfo(
  xp: Long
) extends RawData

final case class RawMergedItems(
  version: Int,
  items: Array[Byte]
) extends RawData

final case class RawInventoryItemInventory(
  contents: Array[RawInventoryItemInventoryContent]
) extends RawData

final case class RawShortcut(
  `type`: Byte,
  targetUniqueId: Long,
  targetReferenceId: Int,
  targetGfxId: Int
) extends RawData

final case class RawShortcutInventory(
  `type`: Byte,
  contents: Array[RawShortcutInventoryContent]
) extends RawData

final case class RawSkill(
  referenceId: Int,
  level: Short,
  xp: Long
) extends RawData

final case class RawSkillInventory(
  contents: Array[RawSkillInventoryContent]
) extends RawData

final case class RawSpellLevel(
  `type`: Byte,
  uniqueId: Long,
  spellId: Int,
  level: Short,
  skills: I8Array[RawSpellLevelSkill]
) extends RawData

final case class RawSpellLevelInventory(
  contents: Array[RawSpellLevelInventoryContent]
) extends RawData

final case class RawMerchantItem(
  item: RawInventoryItem,
  packSize: Short,
  price: Int
) extends RawData

final case class RawMerchantItemInventory(
  uid: Long,
  requiredItemType: Byte,
  nSlots: Short,
  maxPackSize: Byte,
  contents: Array[RawMerchantItemInventoryContent],
  shortAd: String,
  locked: Boolean
) extends RawData

final case class RawEquipableDummy(
  setPackId: Int,
  item: RawInventoryItem
) extends RawData

final case class RawBookcase(
  bookRefIds: Array[RawBookcaseContent]
) extends RawData

final case class RawProperties(
  properties: Array[RawPropertiesProperty]
) extends RawData

final case class RawCharacteristics(
  characteristics: Array[RawCharacteristicsCharacteristic]
) extends RawData

final case class RawInvocationCharacteristic(
  typeid: Short,
  name: String,
  currentHp: Int,
  summonId: Long,
  currentXP: Long,
  cappedLevel: Short,
  forcedLevel: Short,
  obstacleId: Byte,
  DOUBLEINVOC: Option[RawInvocationCharacteristicDOUBLEINVOC],
  IMAGEINVOC: Option[RawInvocationCharacteristicIMAGEINVOC],
  direction: Int,
  summonerId: Long
) extends RawData

final case class RawDoubleInvocationCharacteristic(
  power: Int,
  gfxId: Int,
  sex: Byte,
  haircolorindex: Byte,
  haircolorfactor: Byte,
  skincolorindex: Byte,
  skincolorfactor: Byte,
  pupilcolorindex: Byte,
  clothIndex: Byte,
  faceIndex: Byte,
  doubleType: Byte,
  doublespells: RawSpellLevelInventory,
  doubleCharac: RawCharacteristics,
  equipmentAppareances: Array[RawDoubleInvocationCharacteristicEquipmentAppareance]
) extends RawData

final case class RawImageCharacteristic(
  gfxId: Int,
  sex: Byte,
  imageCharac: RawCharacteristics
) extends RawData

final case class RawStateRunningEffects(
  effects: Array[RawStateRunningEffectsStateRunningEffect]
) extends RawData

final case class RawCapturedCreature(
  index: Byte,
  typeId: Short,
  quantity: Short,
  name: String
) extends RawData

final case class RawSymbiot(
  capturedCreatures: I8Array[RawSymbiotCapturedCreature],
  currentCreatureIndex: Byte
) extends RawData

final case class RawPermissions(
  groupPermissions: Array[RawPermissionsGroupPermission],
  individualPermissions: Array[RawPermissionsIndividualPermission]
) extends RawData

final case class RawBag(
  uniqueId: Long,
  referenceId: Int,
  position: Byte,
  maximumSize: Short,
  inventory: RawInventoryItemInventory
) extends RawData

final case class RawBagContainer(
  bags: Array[RawBagContainerBag]
) extends RawData

final case class RawRoom(
  layoutPosition: Byte,
  interactiveElements: Array[RawRoomContainedInteractiveElement],
  roomSpecificData: RawSpecificRooms
) extends RawData

final case class RawGemControlledRoom(
  primaryGemLocked: Boolean,
  primaryGemitemRefId: Int,
  primaryGemUniqueId: Long,
  secondaryGemitemRefId: Int,
  secondaryGemUniqueId: Long
) extends RawData

final case class RawDimensionalBagForSpawn(
  ownerId: Long,
  ownerName: I8String,
  guildId: Long,
  customViewModelId: Int,
  shelfItems: I8Array[RawDimensionalBagForSpawnShelfItem],
  merchantDisplays: I8Array[RawDimensionalBagForSpawnMerchantDisplayInfo]
) extends RawData

final case class RawRoomPermissions() extends RawData

final case class RawDimensionalBagPermissions(
  dimensionalBagLocked: Boolean,
  groupEntries: Array[RawDimensionalBagPermissionsGroupEntry],
  individualEntries: Array[RawDimensionalBagPermissionsIndividualEntry]
) extends RawData

final case class RawDimensionalBagCraftFee(
  data: Array[Byte]
) extends RawData

final case class RawDimensionalBagPermissionGroupEntry(
  groupType: Byte,
  rights: Byte
) extends RawData

final case class RawDimensionalBagPermissionIndividualEntry(
  userId: Long,
  userName: String,
  rights: Byte
) extends RawData

final case class RawDimensionalBagForSave(
  rooms: I8Array[RawDimensionalBagForSaveRoom],
  cash: Int,
  customViewModelId: Int,
  transactionLog: RawTransactionLog,
  permissions: RawDimensionalBagPermissions,
  ecosystem: RawDimensionalBagEcosystem,
  storageBox: RawDimensionalBagStorageBox,
  craftFee: RawDimensionalBagCraftFee
) extends RawData

final case class RawDimensionalBagStorageBox(
  rawStorageBox: Array[Byte]
) extends RawData

final case class RawStorageBox(
  compartments: Array[RawStorageBoxCompartment]
) extends RawData

final case class RawStorageBoxCompartment(
  id: Int,
  items: Array[RawStorageBoxCompartmentItem]
) extends RawData

final case class RawDimensionalBagForClient(
  ownerId: Long,
  ownerName: String,
  guildId: Long,
  rooms: I8Array[RawDimensionalBagForClientRoom],
  customViewModelId: Int,
  wallet: Option[RawDimensionalBagForClientWallet],
  permissions: RawDimensionalBagPermissions,
  craftFee: RawDimensionalBagCraftFee
) extends RawData

final case class RawDimensionalBagEcosystem(
  lastUpdateTime: Long,
  resources: Array[RawDimensionalBagEcosystemResource]
) extends RawData

final case class RawDimensionalBagResource(
  referenceId: Short,
  step: Byte,
  x: Byte,
  y: Byte,
  z: Byte
) extends RawData

final case class RawAction(
  spawnedCharacter: Option[RawActionSpawnedCharacter]
) extends RawData

final case class RawActionGroup(
  actions: Array[RawActionGroupActionGroup]
) extends RawData

final case class RawScenario(
  scenarioId: Int,
  globalVars: Array[RawScenarioGlobalVar],
  currentVarsForAddedUsers: Array[RawScenarioVarList],
  activeActionGroups: Array[RawScenarioActiveActionGroup],
  executedActionGroups: Array[RawScenarioExecutedActionGroup],
  startTime: Option[RawScenarioStartTime]
) extends RawData

final case class RawChallengeInfo(
  scenarioId: Int,
  activeGoals: Array[RawChallengeInfoActiveGoal],
  executedGoals: Array[RawChallengeInfoExecutedGoal],
  globalVars: Array[RawChallengeInfoGlobalVar],
  watchedVars: Array[RawChallengeInfoWatchedVar],
  remainingTime: Option[RawChallengeInfoRemainingTime]
) extends RawData

final case class RawScenarioManager(
  currentScenarii: Option[RawScenarioManagerCurrentScenarii],
  currentChallengeInfo: Option[RawScenarioManagerCurrentChallengeInfo],
  pastScenarii: Array[RawScenarioManagerPastScenario]
) extends RawData

final case class RawBonusPointCharacteristics(
  freePoints: Short,
  xpBonusPoints: Array[RawBonusPointCharacteristicsXpBonusPoint],
  characteristicBonusPoints: Array[RawBonusPointCharacteristicsCharacteristicBonusPoint]
) extends RawData

final case class RawFlea(
  clientId: Long,
  characterId: Long,
  characterName: String,
  guildId: Long,
  positionX: Int,
  positionY: Int,
  positionZ: Short,
  positionWorldId: Short,
  content: RawDimensionalBagForSave,
  creationDate: Long,
  gameServer: String,
  lastLog: Long
) extends RawData

final case class RawTransactionLog(
  transactions: Array[RawTransactionLogTransaction],
  newTransactionsCount: Short,
  newTransactionsKamas: Long,
  lastReadTransactionDate: Long
) extends RawData

final case class RawGiftPackage(
  title: String,
  message: String,
  contents: Array[RawGiftPackageContent]
) extends RawData

final case class RawGiftItem(
  guid: String,
  referenceId: Int,
  quantity: Short
) extends RawData

final case class RawKrosmozFigure(
  guid: String,
  character: Int,
  pedestal: Int,
  acquiredOn: Long,
  note: String,
  bound: Boolean
) extends RawData

final case class RawEventParticipants(
  participants: Array[RawEventParticipantsParticipant]
) extends RawData

final case class RawEventRegistrations(
  registrations: Array[RawEventRegistrationsRegistration]
) extends RawData

final case class RawProtectorMerchantInventory(
  uid: Long,
  contents: Array[RawProtectorMerchantInventoryContent]
) extends RawData

final case class RawProtectorReferenceInventory(
  contents: Array[RawProtectorReferenceInventoryContent],
  contentsSelection: Option[RawProtectorReferenceInventoryContentsSelection],
  buyableContents: Array[RawProtectorReferenceInventoryBuyableContent]
) extends RawData

final case class RawTax(
  taxContext: Byte,
  taxValue: Float
) extends RawData

final case class RawEcosystemTarget(
  referenceId: Int,
  min: Int,
  max: Int
) extends RawData

final case class RawProtector(
  protectorId: Int,
  nationality: Option[RawProtectorNationality],
  appearance: Option[RawProtectorAppearance],
  challenges: Option[RawProtectorChallenges],
  referenceMerchantInventories: Option[RawProtectorReferenceMerchantInventories],
  nationMerchantInventories: Option[RawProtectorNationMerchantInventories],
  wallet: Option[RawProtectorWallet],
  stake: Option[RawProtectorStake],
  taxes: Option[RawProtectorTaxes],
  weatherModifiers: Option[RawProtectorWeatherModifiers],
  satisfaction: Option[RawProtectorSatisfaction],
  monsterTargets: Option[RawProtectorMonsterTargets],
  resourceTargets: Option[RawProtectorResourceTargets],
  ecosystem: Option[RawProtectorEcosystem]
) extends RawData

final case class RawHavenWorldBuildings(
  buildings: Array[RawHavenWorldBuildingsBuilding]
) extends RawData

final case class RawHavenWorldBuildingsForSpawn(
  buildings: Array[RawHavenWorldBuildingsForSpawnBuilding]
) extends RawData

final case class RawHavenWorldBuildingForSpawn(
  id: Short,
  x: Short,
  y: Short
) extends RawData

final case class RawHavenWorldBuilding(
  uid: Long,
  creationDate: Long,
  id: Short,
  x: Short,
  y: Short,
  equippedSkinItemId: Int
) extends RawData

final case class RawHavenWorldInteractiveElement(
  uid: Long,
  modelId: Int,
  x: Short,
  y: Short,
  z: Short,
  creationDate: Long,
  ownerId: Long,
  buildingUid: Long,
  data: Array[Byte],
  version: Int
) extends RawData

final case class RawHavenWorldTopology(
  originX: Int,
  originY: Int,
  width: Int,
  height: Int,
  partitions: Array[RawHavenWorldTopologyPartitionTopology]
) extends RawData

final case class RawHavenWorldPartitionTopology(
  partitionX: Short,
  partitionY: Short,
  topLeftPatch: Short,
  topRightPatch: Short,
  bottomLeftPatch: Short,
  bottomRightPatch: Short
) extends RawData

final case class RawNationGovernmentDataGovernor(
  titleId: Short,
  nbMandate: Int
) extends RawData

final case class RawNationLawLaw(
  lawId: Long
) extends RawData

final case class RawNationDiplomacyAlignment(
  nationId: Int,
  alignment: Byte
) extends RawData

final case class RawNationDiplomacyRequest(
  nationId: Int,
  alignment: Byte
) extends RawData

final case class RawNationEconomySpending(
  itemTypeId: Byte,
  amount: Int
) extends RawData

final case class RawNationEconomyProfit(
  taxId: Byte,
  amount: Int
) extends RawData

final case class RawNationEconomyTotalCash(
  cash: Int
) extends RawData

final case class RawNationSurveyOpinion(
  opinionId: Byte,
  nbBallots: Int
) extends RawData

final case class RawGuildStorageCompartmentItem(
  position: Short,
  item: RawInventoryItem
) extends RawData

final case class RawGuildStorageCompartmentBuilding(
  buildingUid: Long
) extends RawData

final case class RawGuildStorageHistoryHistoryItemEntry(
  memberName: String,
  date: Long,
  qty: Short,
  item: RawInventoryItem
) extends RawData

final case class RawGuildStorageHistoryHistoryMoneyEntry(
  memberName: String,
  date: Long,
  amount: Int
) extends RawData

final case class RawVaultItem(
  position: Short,
  item: RawInventoryItem
) extends RawData

final case class RawCosmeticsInventoryHandlerActiveCosmetic(
  activeCosmeticRefId: Int
) extends RawData

final case class RawLocksLock(
  lockId: Int,
  lockDate: Long,
  currentLockValue: Int,
  currentLockValueLastChange: Long
) extends RawData

final case class RawQuestItemInventoryRawQuestItem(
  refId: Int,
  quantity: Short
) extends RawData

final case class RawCosmeticsItemInventoryRawCosmeticsItem(
  refId: Int,
  bindId: Byte
) extends RawData

final case class RawInventoryItemPet(
  rawPet: RawPet
) extends RawData

final case class RawInventoryItemXp(
  rawXp: RawItemXp
) extends RawData

final case class RawInventoryItemGems(
  rawGems: RawGems
) extends RawData

final case class RawInventoryItemRentInfo(
  rawRentInfo: RawRentInfo
) extends RawData

final case class RawInventoryItemCompanionInfo(
  rawCompanionInfo: RawCompanionInfo
) extends RawData

final case class RawInventoryItemBind(
  rawItemBind: RawItemBind
) extends RawData

final case class RawInventoryItemElements(
  rawItemElements: RawItemElements
) extends RawData

final case class RawInventoryItemMergedItems(
  rawMergedItems: RawMergedItems
) extends RawData

final case class RawInventoryItemMimiSymbic(
  skinItemRefId: Int
) extends RawData

final case class RawItemOptionalInfoPet(
  rawPet: RawPet
) extends RawData

final case class RawItemOptionalInfoXp(
  rawXp: RawItemXp
) extends RawData

final case class RawItemOptionalInfoGems(
  rawGems: RawGems
) extends RawData

final case class RawItemOptionalInfoCompanion(
  rawCompanion: RawCompanionInfo
) extends RawData

final case class RawGemsContent(
  position: Byte,
  referenceId: Int
) extends RawData

final case class RawInventoryItemInventoryContent(
  position: Short,
  item: RawInventoryItem
) extends RawData

final case class RawShortcutInventoryContent(
  position: Short,
  shortcut: RawShortcut
) extends RawData

final case class RawSkillInventoryContent(
  skill: RawSkill
) extends RawData

final case class RawSpellLevelSkill(
  skillId: Int
) extends RawData

final case class RawSpellLevelInventoryContent(
  spellLevel: RawSpellLevel
) extends RawData

final case class RawMerchantItemInventoryContent(
  position: Short,
  merchantItem: RawMerchantItem
) extends RawData

final case class RawBookcaseContent(
  bookRefId: Int
) extends RawData

final case class RawPropertiesProperty(
  id: Byte,
  count: Byte
) extends RawData

final case class RawCharacteristicsCharacteristic(
  index: Byte,
  current: Int,
  min: Int,
  max: Int,
  maxPercentModifier: Int
) extends RawData

final case class RawInvocationCharacteristicDOUBLEINVOC(
  doubledata: RawDoubleInvocationCharacteristic
) extends RawData

final case class RawInvocationCharacteristicIMAGEINVOC(
  imagedata: RawImageCharacteristic
) extends RawData

final case class RawDoubleInvocationCharacteristicEquipmentAppareance(
  position: Byte,
  refId: Int
) extends RawData

final case class RawStateRunningEffectsStateRunningEffect(
  uid: Long,
  stateBaseId: Short,
  level: Short,
  remainingDurationInMs: Int,
  startDate: Long
) extends RawData

final case class RawSymbiotCapturedCreature(
  capturedCreature: RawCapturedCreature
) extends RawData

final case class RawPermissionsGroupPermission(
  groupId: Byte,
  permissions: Short
) extends RawData

final case class RawPermissionsIndividualPermission(
  characterName: String,
  permissions: Short
) extends RawData

final case class RawBagContainerBag(
  bag: RawBag
) extends RawData

final case class RawRoomContainedInteractiveElement(
  persistantData: RawInteractiveElementPersistantData
) extends RawData

final case class RawDimensionalBagForSpawnShelfItem(
  shelfItem: RawMerchantItem
) extends RawData

final case class RawDimensionalBagForSpawnMerchantDisplayInfo(
  itemType: Byte,
  contentQuantity: Byte,
  contentQuality: Byte
) extends RawData

final case class RawDimensionalBagPermissionsGroupEntry(
  entry: RawDimensionalBagPermissionGroupEntry
) extends RawData

final case class RawDimensionalBagPermissionsIndividualEntry(
  entry: RawDimensionalBagPermissionIndividualEntry
) extends RawData

final case class RawDimensionalBagForSaveRoom(
  room: RawRoom
) extends RawData

final case class RawStorageBoxCompartmentItem(
  position: Short,
  item: RawInventoryItem
) extends RawData

final case class RawDimensionalBagForClientRoom(
  room: RawRoom
) extends RawData

final case class RawDimensionalBagForClientWallet(
  cash: Int
) extends RawData

final case class RawDimensionalBagEcosystemResource(
  resource: RawDimensionalBagResource
) extends RawData

final case class RawActionSpawnedCharacter(
  serializedCharacter: Array[Byte]
) extends RawData

final case class RawActionGroupActionGroup(
  actionUid: Int,
  action: RawAction
) extends RawData

final case class RawScenarioGlobalVar(
  varId: Byte,
  value: Long
) extends RawData

final case class RawScenarioUserVar(
  varId: Byte,
  value: Long
) extends RawData

final case class RawScenarioVarList(
  userId: Long,
  vars: Array[RawScenarioUserVar]
) extends RawData

final case class RawScenarioActiveActionGroup(
  actionGroupId: Int
) extends RawData

final case class RawScenarioExecutedActionGroup(
  actionGroupId: Int,
  actionGroup: RawActionGroup
) extends RawData

final case class RawScenarioStartTime(
  value: Long
) extends RawData

final case class RawChallengeInfoActiveGoal(
  actionGroupId: Int
) extends RawData

final case class RawChallengeInfoExecutedGoal(
  actionGroupId: Int
) extends RawData

final case class RawChallengeInfoGlobalVar(
  varId: Byte,
  value: Long
) extends RawData

final case class RawChallengeInfoWatchedVar(
  varId: Byte,
  value: Long
) extends RawData

final case class RawChallengeInfoRemainingTime(
  value: Long
) extends RawData

final case class RawScenarioManagerCurrentScenario(
  scenario: RawScenario
) extends RawData

final case class RawScenarioManagerCurrentScenarii(
  scenarii: Array[RawScenarioManagerCurrentScenario]
) extends RawData

final case class RawScenarioManagerChallengeInfo(
  challenge: RawChallengeInfo
) extends RawData

final case class RawScenarioManagerCurrentChallengeInfo(
  challenges: Array[RawScenarioManagerChallengeInfo]
) extends RawData

final case class RawScenarioManagerPastScenario(
  scenarioId: Int,
  executionCount: Short,
  status: Byte
) extends RawData

final case class RawBonusPointCharacteristicsXpBonusPoint(
  characId: Byte,
  nbPoint: Short
) extends RawData

final case class RawBonusPointCharacteristicsCharacteristicBonusPoint(
  characId: Byte,
  value: Short
) extends RawData

final case class RawTransactionLogSoldItem(
  refId: Int,
  quantity: Short
) extends RawData

final case class RawTransactionLogTransaction(
  transactionDate: Long,
  buyerId: Long,
  buyerName: I8String,
  soldItems: I8Array[RawTransactionLogSoldItem],
  totalPrice: Long
) extends RawData

final case class RawGiftPackageContent(
  giftItem: RawGiftItem
) extends RawData

final case class RawEventParticipantsParticipant(
  participantId: Long,
  participantName: String
) extends RawData

final case class RawEventRegistrationsRegistration(
  registeredId: Long,
  registeredName: String
) extends RawData

final case class RawProtectorMerchantInventoryContent(
  position: Short,
  `type`: Byte,
  featureReferenceId: Int,
  duration: Long,
  startDate: Long,
  merchantItem: RawMerchantItem
) extends RawData

final case class RawProtectorReferenceInventoryContent(
  referenceId: Int,
  remainingDuration: Int
) extends RawData

final case class RawProtectorReferenceInventoryContentSelection(
  referenceId: Int
) extends RawData

final case class RawProtectorReferenceInventoryContentsSelection(
  contentsSelection: Array[RawProtectorReferenceInventoryContentSelection]
) extends RawData

final case class RawProtectorReferenceInventoryBuyableContent(
  referenceId: Int,
  price: Int
) extends RawData

final case class RawProtectorNationality(
  nativeNationId: Int,
  currentNationId: Int,
  territoryId: Int
) extends RawData

final case class RawProtectorAppearance(
  monsterCrewId: Int,
  monterId: Long
) extends RawData

final case class RawProtectorChallenges(
  dropTableId: Int,
  dropTableIdToBuy: Int,
  dropTableIdChaos: Int
) extends RawData

final case class RawProtectorReferenceMerchantInventories(
  challengeReferenceInventory: RawProtectorReferenceInventory,
  climateReferenceInventory: RawProtectorReferenceInventory,
  buffsReferenceInventory: RawProtectorReferenceInventory,
  itemsReferenceInventory: RawProtectorReferenceInventory
) extends RawData

final case class RawProtectorNationMerchantInventories(
  challengeMerchantInventory: RawProtectorMerchantInventory,
  climateMerchantInventory: RawProtectorMerchantInventory,
  buffsMerchantInventory: RawProtectorMerchantInventory,
  itemsMerchantInventory: RawProtectorMerchantInventory
) extends RawData

final case class RawProtectorContent(
  contextId: Byte,
  cashAmount: Int
) extends RawData

final case class RawProtectorWallet(
  cashAmount: Int,
  contexts: Array[RawProtectorContent]
) extends RawData

final case class RawProtectorStake(
  fightStake: Int
) extends RawData

final case class RawProtectorTaxValue(
  tax: RawTax
) extends RawData

final case class RawProtectorTaxes(
  taxValues: Array[RawProtectorTaxValue]
) extends RawData

final case class RawProtectorWeatherModifier(
  applicationDate: Long,
  climateBonusId: Int
) extends RawData

final case class RawProtectorWeatherModifiers(
  modifiers: Array[RawProtectorWeatherModifier]
) extends RawData

final case class RawProtectorSatisfaction(
  satisfactionLevel: Byte
) extends RawData

final case class RawProtectorMonsterTarget(
  target: RawEcosystemTarget
) extends RawData

final case class RawProtectorMonsterTargets(
  targets: Array[RawProtectorMonsterTarget]
) extends RawData

final case class RawProtectorResourceTarget(
  target: RawEcosystemTarget
) extends RawData

final case class RawProtectorResourceTargets(
  targets: Array[RawProtectorResourceTarget]
) extends RawData

final case class RawProtectorMonsterFamily(
  familyId: Int
) extends RawData

final case class RawProtectorResourceFamily(
  familyId: Int
) extends RawData

final case class RawProtectorEcosystem(
  protectedMonsters: Array[RawProtectorMonsterFamily],
  protectedResources: Array[RawProtectorResourceFamily]
) extends RawData

final case class RawHavenWorldBuildingsBuilding(
  building: RawHavenWorldBuilding
) extends RawData

final case class RawHavenWorldBuildingsForSpawnBuilding(
  building: RawHavenWorldBuildingForSpawn
) extends RawData

final case class RawHavenWorldTopologyPartitionTopology(
  partition: RawHavenWorldPartitionTopology
) extends RawData
