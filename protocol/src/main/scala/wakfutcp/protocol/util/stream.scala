package wakfutcp.protocol.util

import java.nio.ByteOrder

import akka.NotUsed
import akka.actor.typed._
import akka.actor.typed.scaladsl._
import akka.stream.scaladsl.{Broadcast, Flow, Framing, GraphDSL, Sink}
import akka.stream.typed.scaladsl._
import akka.stream.{FlowShape, OverflowStrategy}
import akka.util.ByteString
import wakfutcp.protocol.config.CodecPath
import wakfutcp.protocol.{ClientMessage, Decoder, ServerMessage, config}

import scala.util.{Failure, Success}

object stream {
  def withConnection[T](later: ActorRef[ByteString] => Behavior[Message[T]]): Behavior[Message[T]] =
    Behaviors.immutable[Message[T]] { (ctx, msg) =>
      msg match {
        case ConnectionEstablished(connection) =>
          ctx.watch(connection)
          later(connection)
        case _ =>
          Behaviors.same
      }
    }

  object client {
    def flow(handler: ActorRef[Message[ServerMessage]]): Flow[ByteString, ByteString, ActorRef[ByteString]] =
      stream.flow(handler)(parseFlow)

    private val codecs: Map[Short, Decoder[ServerMessage]] =
      config.client.codecs.mapValues(loadCodecInstance[ServerMessage])

    private val parseFlow: Flow[ByteString, ServerMessage, NotUsed] =
      Flow[ByteString]
        .via(framing)
        .map { message =>
          val bb = message.asByteBuffer
          bb.position(2) // skip length
        val id = bb.getShort
          codecs.get(id)
            .map(_.decode(bb))
            .getOrElse(UnknownServerMessage(id))
        }

    final case class UnknownServerMessage(id: Short) extends ServerMessage
  }

  object server {
    def flow(handler: ActorRef[Message[ClientMessage]]): Flow[ByteString, ByteString, ActorRef[ByteString]] =
      stream.flow(handler)(parseFlow)

    private val codecs: Map[Short, Decoder[ClientMessage]] =
      config.server.codecs.mapValues(loadCodecInstance[ClientMessage])

    private val parseFlow: Flow[ByteString, ClientMessage, NotUsed] =
      Flow[ByteString]
        .via(framing)
        .map { message =>
          val bb = message.asByteBuffer
          bb.position(2) // skip length
          val arch = bb.get
          val id = bb.getShort
          codecs.get(id)
            .map(_.decode(bb))
            .getOrElse(UnknownClientMessage(id, arch))
        }

    final case class UnknownClientMessage(id: Short, arch: Byte) extends ClientMessage
  }

  private val framing = Framing.lengthField(2, 0, Int.MaxValue, ByteOrder.BIG_ENDIAN, (_, i) => i)

  private def flow[T](handler: ActorRef[Message[T]])
                     (parser: Flow[ByteString, T, NotUsed]): Flow[ByteString, ByteString, ActorRef[ByteString]] = {
    val source =
      ActorSource.actorRef[ByteString](PartialFunction.empty, PartialFunction.empty, 128, OverflowStrategy.fail)
    Flow.fromGraph(GraphDSL.create(source) { implicit b => shape =>
      import GraphDSL.Implicits._

      val register = Sink.foreach[ActorRef[ByteString]] { ref => handler ! ConnectionEstablished(ref) }
      val outbound = b.add(Flow[ByteString])
      val inbound = b.add(parser)

      b.materializedValue ~> register
      inbound ~> Sink.foreach[T](handler ! Recv(_))
      shape ~> outbound
      FlowShape.of(inbound.in, outbound.out)
    })
  }

  // scalastyle:off
  private def loadCodecInstance[T](path: CodecPath): Decoder[T] = {
    val singleton = Class
      .forName(path.singleton)
      .getDeclaredField("MODULE$")
      .get(null)
    singleton.getClass
      .getDeclaredMethod(path.field)
      .invoke(singleton)
      .asInstanceOf[Decoder[T]]
  }
  // scalastyle:on

  sealed trait Message[+T]
  final case class ConnectionEstablished(connection: ActorRef[ByteString]) extends Message[Nothing]
  final case class ConnectionClosed(exception: Option[Throwable]) extends Message[Nothing]
  final case class Recv[+T](message: T) extends Message[T]
}
