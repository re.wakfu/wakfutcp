package wakfutcp.protocol.messages.server

import wakfutcp.protocol.protobuf.buildSheet.ProtoBuildSheetSet
import wakfutcp.protocol.{Codec, ServerMessage}

final case class BuildSheetNotificationMessage(
  characterId: Long,
  protoBuildSheetSet: ProtoBuildSheetSet
) extends ServerMessage {
  override val id = 8427
}

object BuildSheetNotificationMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[BuildSheetNotificationMessage] =
    (long, block(int, protobuf(ProtoBuildSheetSet)))
      .imapN(apply)(Function.unlift(unapply))
}
