package wakfutcp.protocol.messages.server

import java.nio.ByteBuffer

import wakfutcp.protocol.common.Position
import wakfutcp.protocol.{Decoder, ServerMessage}

final case class FightersPlacementPositionMessage(
  fightId: Int,
  fighters: Array[FighterPosition],
  shouldTeleport: Boolean
) extends ServerMessage {
  override val id = 7906
}

final case class FighterPosition(id: Long, position: Position)

object FightersPlacementPositionMessage {
  implicit val decoder: Decoder[FightersPlacementPositionMessage] = (bb: ByteBuffer) =>
    FightersPlacementPositionMessage(
      bb.getInt,
      Array.fill(bb.getShort.toInt) {
        FighterPosition(bb.getLong, Decoder[Position].decode(bb))
      },
      bb.get == 1
  )
}
