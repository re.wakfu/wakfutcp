package wakfutcp.protocol.messages.server

import java.nio.ByteBuffer

import wakfutcp.protocol.{Decoder, ServerMessage}

final case class FighterTurnEndMessage(
  fightId: Long,
  fighterId: Long,
  timeScoreGain: Int,
  addedRemainingSeconds: Int
) extends ServerMessage {
  override val id = 8106
}

object FighterTurnEndMessage {
  implicit val decoder: Decoder[FighterTurnEndMessage] = (bb: ByteBuffer) => {
    val fightId = bb.getLong
    bb.getInt
    bb.getInt
    FighterTurnEndMessage(
      fightId,
      bb.getLong,
      bb.getInt,
      bb.getInt
    )
  }
}
