package wakfutcp.protocol.messages.server

import wakfutcp.protocol.{Codec, ServerMessage}

final case class NotificationFriendOnlineMessage(
  friendName: String,
  characterName: String,
  commentary: String,
  userId: Long,
  breedId: Short,
  sex: Byte,
  xp: Long
) extends ServerMessage {
  override val id = 3148
}

object NotificationFriendOnlineMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[NotificationFriendOnlineMessage] =
    (utf8(ubyte), utf8(ubyte), utf8(ubyte), long, short, byte, long)
      .imapN(apply)(Function.unlift(unapply))
}
