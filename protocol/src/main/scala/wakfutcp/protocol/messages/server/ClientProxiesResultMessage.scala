package wakfutcp.protocol.messages.server

import wakfutcp.protocol.common.WorldInfo
import wakfutcp.protocol.{Codec, ServerMessage, common}

final case class ClientProxiesResultMessage(
  proxies: Array[wakfutcp.protocol.common.Proxy],
  worlds: Array[WorldInfo]
) extends ServerMessage {
  override val id = 1036
}

object ClientProxiesResultMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[ClientProxiesResultMessage] =
    (array(int, common.Proxy.codec), array(int, WorldInfo.codec))
      .imapN(apply)(Function.unlift(unapply))
}
