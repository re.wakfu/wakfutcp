package wakfutcp.protocol.messages.server

import wakfutcp.protocol.{Codec, ServerMessage}

final case class CharacterDeletionResultMessage(
  characterId: Long,
  successful: Boolean
) extends ServerMessage {
  override val id = 2052
}
object CharacterDeletionResultMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[CharacterDeletionResultMessage] =
    (long, bool).imapN(apply)(Function.unlift(unapply))
}
