package wakfutcp.protocol.messages.server

import java.nio.ByteBuffer

import akka.util.ByteStringBuilder
import wakfutcp.protocol.{Codec, ServerMessage}

final case class IgnoreListMessage(ignored: Seq[IgnoredPlayer]) extends ServerMessage {
  override val id = 3146
}

final case class IgnoredPlayer(id: Long, name: String, characterName: String)

object IgnoreListMessage {
  implicit val codec: Codec[IgnoreListMessage] = new Codec[IgnoreListMessage] {

    import wakfutcp.protocol.util.extensions._
    import wakfutcp.protocol.Encoder.DefaultOrder

    override def decode(bb: ByteBuffer): IgnoreListMessage = {
      val length = bb.get
      val entries = Seq.fill(length.toInt) {
        val name = bb.getUTF8_u8
        (bb.getLong, name)
      }
      IgnoreListMessage(
        entries.map {
          case (id, name) =>
            IgnoredPlayer(id, name, bb.getUTF8_u8)
        }
      )
    }

    override def encode(
      builder: ByteStringBuilder,
      t: IgnoreListMessage
    ): ByteStringBuilder = {
      builder.putByte(t.ignored.length.toByte)
      t.ignored.foreach { i =>
        builder.putUTF8_u8(i.name)
        builder.putLong(i.id)
      }
      t.ignored.foreach { i =>
        builder.putUTF8_u8(i.characterName)
      }
      builder
    }
  }
}
