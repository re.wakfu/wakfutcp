package wakfutcp.protocol.messages.server

import java.nio.ByteBuffer

import akka.util.ByteStringBuilder
import wakfutcp.protocol.{Codec, ServerMessage}

import scala.concurrent.duration._

sealed trait ClientAuthenticationResultsMessage extends ServerMessage {
  override val id = 1025

  def code: Byte
}

object ClientAuthenticationResultsMessage {
  implicit val codec: Codec[ClientAuthenticationResultsMessage] =
    new Codec[ClientAuthenticationResultsMessage] {
      import wakfutcp.protocol.Encoder.DefaultOrder

      def decode(bb: ByteBuffer): ClientAuthenticationResultsMessage = {
        val resultCode = bb.get
        resultCode match {
          case 0 ⇒ Success(Codec.byteArray(Codec.short).decode(bb))
          case 2 ⇒ InvalidLogin
          case 3 ⇒ AlreadyConnected
          case 5 ⇒ AccountBanned(bb.getInt.minutes)
          case 9 ⇒ ServerLocked
          case 10 ⇒ LoginServerDown
          case 11 ⇒ TooManyConnections
          case 40 ⇒ InvalidLogin
          case 42 ⇒ InvalidToken
          case _ ⇒ ???
        }
      }

      override def encode(builder: ByteStringBuilder,
                          t: ClientAuthenticationResultsMessage): ByteStringBuilder = {
        builder.putByte(t.code)
        t match {
          case Success(data) =>
            builder
//            accountBlockCodec.encode(builder, data)
          case AccountBanned(duration) =>
            builder.putInt(duration.toMinutes.toInt)
          case _ => builder
        }
      }
    }

  final case class AccountBanned(
    banDuration: FiniteDuration
  ) extends ClientAuthenticationResultsMessage {
    override def code: Byte = 5
  }

  final case class Success(
    accountInformations: Array[Byte]
  ) extends ClientAuthenticationResultsMessage {
    override def code: Byte = 0
  }

  case object ServerLocked extends ClientAuthenticationResultsMessage {
    override def code: Byte = 9
  }

  case object InvalidLogin extends ClientAuthenticationResultsMessage {
    override def code: Byte = 40
  }

  case object AlreadyConnected extends ClientAuthenticationResultsMessage {
    override def code: Byte = 3
  }

  case object LoginServerDown extends ClientAuthenticationResultsMessage {
    override def code: Byte = 10
  }

  case object TooManyConnections extends ClientAuthenticationResultsMessage {
    override def code: Byte = 11
  }

  case object InvalidToken extends ClientAuthenticationResultsMessage {
    override def code: Byte = 42
  }

}
