package wakfutcp.protocol.messages.server

import wakfutcp.protocol.{Codec, ServerMessage}

final case class FreeCompanionBreedIdMessage(
  freeCompanionBreedId: Short
) extends ServerMessage {
  override val id = 2078
}

object FreeCompanionBreedIdMessage {
  import Codec._
  import cats.syntax.invariant._

  implicit val codec: Codec[FreeCompanionBreedIdMessage] =
    short.imap(apply)(Function.unlift(unapply))
}
