package wakfutcp.protocol.messages.server

import wakfutcp.protocol.{Codec, ServerMessage}

final case class SpellDeckSynchronizeMessage(
  playerId: Long,
  deckIndex: Int,
  level: Short,
  activeSpells: Array[Int],
  passiveSpells: Array[Int]
) extends ServerMessage {
  override val id = 13252
}

object SpellDeckSynchronizeMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[SpellDeckSynchronizeMessage] =
    (long, int, short, array(int, int), array(int, int))
      .imapN(apply)(Function.unlift(unapply))
}
