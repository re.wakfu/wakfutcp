package wakfutcp.protocol.messages.server

import wakfutcp.protocol.{Codec, ServerMessage}

final case class InteractiveElementUpdateMessage(
  elementId: Long,
  sharedData: Array[Byte] // TODO: decode this
) extends ServerMessage {
  override val id = 202
}

object InteractiveElementUpdateMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[InteractiveElementUpdateMessage] =
    (long, byteArray(ushort))
      .imapN(apply)(Function.unlift(unapply))
}
