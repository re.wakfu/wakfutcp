package wakfutcp.protocol.messages.server

import wakfutcp.protocol.{Codec, ServerMessage}

final case class ClientCalendarSynchronizationMessage(
  synchronizationTime: Long
) extends ServerMessage {
  override val id = 2063
}

object ClientCalendarSynchronizationMessage {
  import Codec._
  import cats.syntax.invariant._

  implicit val codec: Codec[ClientCalendarSynchronizationMessage] =
    long.imap(apply)(Function.unlift(unapply))
}
