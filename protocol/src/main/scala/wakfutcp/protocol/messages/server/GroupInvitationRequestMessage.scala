package wakfutcp.protocol.messages.server

import wakfutcp.protocol.{Codec, ServerMessage}

final case class GroupInvitationRequestMessage(
  groupType: Byte,
  inviterName: String,
  inviterId: Long,
  fromPartySearch: Boolean
) extends ServerMessage {
  override val id = 502
}

object GroupInvitationRequestMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[GroupInvitationRequestMessage] =
    (byte, utf8(byte), long, bool).imapN(apply)(Function.unlift(unapply))
}
