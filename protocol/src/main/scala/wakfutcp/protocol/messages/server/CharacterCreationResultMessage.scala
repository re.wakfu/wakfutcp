package wakfutcp.protocol.messages.server

import enumeratum.values.{ByteEnum, ByteEnumEntry}
import wakfutcp.protocol.{Codec, ServerMessage}

import scala.collection.immutable

sealed abstract class CharacterCreationResultMessage(val value: Byte)
    extends ByteEnumEntry
    with ServerMessage {
  override val id = 2054
}

object CharacterCreationResultMessage extends ByteEnum[CharacterCreationResultMessage] {

  import Codec._
  import cats.syntax.invariant._

  implicit val codec: Codec[CharacterCreationResultMessage] =
    byte.imap(withValueOpt(_).getOrElse(Unknown))(_.value)

  case object Success extends CharacterCreationResultMessage(0)
  case object NameIsTaken extends CharacterCreationResultMessage(10)
  case object NameIsInvalid extends CharacterCreationResultMessage(11)
  case object TooManyCharacters extends CharacterCreationResultMessage(12)
  case object Unknown extends CharacterCreationResultMessage(-1)

  def values: immutable.IndexedSeq[CharacterCreationResultMessage] = findValues
}
