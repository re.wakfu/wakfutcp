package wakfutcp.protocol.messages.server

import wakfutcp.protocol.{Codec, ServerMessage}

final case class ItemIdCacheUpdateMessage(
  squashing: Boolean,
  uids: Array[Long]
) extends ServerMessage {
  override val id = 5300
}

object ItemIdCacheUpdateMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[ItemIdCacheUpdateMessage] =
    (bool, array(byte, long))
      .imapN(apply)(Function.unlift(unapply))
}
