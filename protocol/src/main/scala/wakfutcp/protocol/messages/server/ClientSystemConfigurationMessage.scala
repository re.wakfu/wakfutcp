package wakfutcp.protocol.messages.server

import wakfutcp.protocol.common.SystemConfigType
import wakfutcp.protocol.{Codec, ServerMessage}

final case class ClientSystemConfigurationMessage(
  properties: Map[SystemConfigType, String]
) extends ServerMessage {
  override val id = 2067
}

object ClientSystemConfigurationMessage {
  import Codec._
  import cats.syntax.invariant._

  implicit val codec: Codec[ClientSystemConfigurationMessage] =
    block(int, map(int, SystemConfigType.codec, utf8(int)))
      .imap(apply)(Function.unlift(unapply))
}
