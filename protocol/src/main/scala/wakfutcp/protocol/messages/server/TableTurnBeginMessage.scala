package wakfutcp.protocol.messages.server

import java.nio.ByteBuffer

import wakfutcp.protocol.{Decoder, ServerMessage}

final case class TableTurnBeginMessage(
  numTurns: Short,
  timeline: Array[Byte]
) extends ServerMessage {
  override val id = 8100
}

object TableTurnBeginMessage {
  import wakfutcp.protocol.util.extensions._

  implicit val decoder: Decoder[TableTurnBeginMessage] = (bb: ByteBuffer) => {
    bb.getInt
    bb.getInt
    bb.getInt
    TableTurnBeginMessage(
      bb.getShort,
      if (bb.remaining() > 0) bb.getByteArray_16
      else Array.empty
    )
  }
}
