package wakfutcp.protocol.messages.server

import wakfutcp.protocol.{Codec, ServerMessage}

final case class GroupResultMessage(
  groupType: Byte,
  result: Int
) extends ServerMessage {
  override val id = 504
}

object GroupResultMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[GroupResultMessage] =
    (byte, int).imapN(apply)(Function.unlift(unapply))
}
