package wakfutcp.protocol.messages.server

import wakfutcp.protocol.messages.server.ActorEquipmentUpdateMessage.ChangedItem
import wakfutcp.protocol.{Codec, ServerMessage}

final case class ActorEquipmentUpdateMessage(
  actorId: Long,
  changedItems: Map[Byte, ChangedItem]
) extends ServerMessage {
  override val id = 5206
}

object ActorEquipmentUpdateMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[ActorEquipmentUpdateMessage] =
    (long, map(byte, byte, ChangedItem.codec))
      .imapN(apply)(Function.unlift(unapply))

  final case class ChangedItem(itemId: Int, skinId: Int)

  object ChangedItem {
    implicit val codec: Codec[ChangedItem] =
      (int, int).imapN(apply)(Function.unlift(unapply))
  }
}
