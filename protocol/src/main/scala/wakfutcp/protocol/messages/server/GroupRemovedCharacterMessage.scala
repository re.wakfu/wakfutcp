package wakfutcp.protocol.messages.server

import wakfutcp.protocol.{Codec, ServerMessage}

final case class GroupRemovedCharacterMessage(
  groupId: Long,
  characterRemovedIds: Array[Long]
) extends ServerMessage {
  override val id = 506
}

object GroupRemovedCharacterMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[GroupRemovedCharacterMessage] =
    (long, repeatUntilEnd(long))
      .imapN(apply)(Function.unlift(unapply))
}
