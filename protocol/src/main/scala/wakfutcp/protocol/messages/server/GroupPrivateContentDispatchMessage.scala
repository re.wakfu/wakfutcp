package wakfutcp.protocol.messages.server

import wakfutcp.protocol.{Codec, ServerMessage}

final case class GroupPrivateContentDispatchMessage(
  groupId: Long,
  sender: String,
  senderId: Long,
  content: String
) extends ServerMessage {
  override val id = 508
}

object GroupPrivateContentDispatchMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[GroupPrivateContentDispatchMessage] =
    (long, utf8(short), long, utf8(short))
      .imapN(apply)(Function.unlift(unapply))
}
