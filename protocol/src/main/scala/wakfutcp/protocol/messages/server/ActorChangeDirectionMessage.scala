package wakfutcp.protocol.messages.server

import wakfutcp.protocol.common.Direction
import wakfutcp.protocol.{Codec, ServerMessage}

final case class ActorChangeDirectionMessage(
  playerId: Long,
  direction: Direction
) extends ServerMessage {
  override val id = 4118
}

object ActorChangeDirectionMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[ActorChangeDirectionMessage] =
    (long, Direction.codec)
      .imapN(apply)(Function.unlift(unapply))
}
