package wakfutcp.protocol.messages.server

import cats.syntax.apply._
import enumeratum.values.{ByteEnum, ByteEnumEntry}
import wakfutcp.protocol.common.Community
import wakfutcp.protocol.messages.server.ClientDispatchAuthenticationResultMessage.Result
import wakfutcp.protocol.protobuf.account._
import wakfutcp.protocol.{Codec, ServerMessage}

import scala.collection.immutable

final case class ClientDispatchAuthenticationResultMessage(
  result: Result,
  account: Option[AccountInformation]
) extends ServerMessage {
  override val id = 1027
}

final case class AccountInformation(
  community: Community,
  adminInformation: Option[AdminInformation],
  status: Status
)

object AccountInformation {
  import Codec._

  implicit val codec: Codec[AccountInformation] =
    (Community.codec, option(AdminInformation.codec), protobuf(Status))
      .imapN(apply)(Function.unlift(unapply))
}

final case class AdminInformation(
  account: Long,
  name: String,
  rights: Array[AdminRight]
)

object AdminInformation {
  import Codec._

  implicit val codec: Codec[AdminInformation] =
    (long, utf8(int), array(int, AdminRight.codec))
      .imapN(apply)(Function.unlift(unapply))
}

final case class AdminRight(server: Int, right: Int)

object AdminRight {
  import Codec._

  implicit val codec: Codec[AdminRight] =
    (int, int).imapN(apply)(Function.unlift(unapply))
}

object ClientDispatchAuthenticationResultMessage {
  import Codec._

  implicit val codec: Codec[ClientDispatchAuthenticationResultMessage] =
    (Result.codec, option(AccountInformation.codec))
      .imapN(apply)(Function.unlift(unapply))

  sealed abstract class Result(val value: Byte) extends ByteEnumEntry

  object Result extends ByteEnum[Result] {
    import cats.syntax.invariant._

    implicit val codec: Codec[Result] =
      byte.imap(withValueOpt(_).getOrElse(Unknown))(_.value)

    case object Success extends Result(0)
    case object InternalError extends Result(7)
    case object Banned extends Result(5)
    case object InvalidLogin extends Result(2)
    case object ForbiddenCommunity extends Result(25)
    case object Unknown extends Result(-1)

    def values: immutable.IndexedSeq[Result] = findValues

  }

}
