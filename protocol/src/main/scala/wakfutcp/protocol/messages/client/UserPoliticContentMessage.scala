package wakfutcp.protocol.messages.client

import wakfutcp.protocol.{ClientMessage, Codec}

final case class UserPoliticContentMessage(
  content: String
) extends ClientMessage {
  override val id = 3163
  override val arch = 6
}

object UserPoliticContentMessage {
  import Codec._
  import cats.syntax.invariant._

  implicit val codec: Codec[UserPoliticContentMessage] =
    utf8(byte).imap(apply)(Function.unlift(unapply))
}
