package wakfutcp.protocol.messages.client

import wakfutcp.protocol.{ClientMessage, Codec}

final case class GiftInventoryRequestMessage(
  lang: String // this should be of length equal to 2
) extends ClientMessage {
  override val id = 13001
  override val arch = 3
}

object GiftInventoryRequestMessage {
  import Codec._
  import cats.syntax.invariant._

  implicit val codec: Codec[GiftInventoryRequestMessage] =
    utf8C(2).imap(apply)(Function.unlift(unapply))
}
