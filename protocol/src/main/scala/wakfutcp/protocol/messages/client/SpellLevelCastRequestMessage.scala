package wakfutcp.protocol.messages.client

import wakfutcp.protocol.common.Position
import wakfutcp.protocol.{ClientMessage, Codec}

final case class SpellLevelCastRequestMessage(
  fighterId: Long,
  spellId: Long,
  castAt: Position
) extends ClientMessage {
  override val id = 8109
  override val arch = 3
}

object SpellLevelCastRequestMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[SpellLevelCastRequestMessage] =
    (long, long, Position.codec)
      .imapN(apply)(Function.unlift(unapply))
}
