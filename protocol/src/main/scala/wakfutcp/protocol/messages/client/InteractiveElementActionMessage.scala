package wakfutcp.protocol.messages.client

import wakfutcp.protocol.{ClientMessage, Codec}

final case class InteractiveElementActionMessage(
  elementId: Long,
  actionId: Short
) extends ClientMessage {
  override val id = 201
  override val arch = 3
}

object InteractiveElementActionMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[InteractiveElementActionMessage] =
    (long, short).imapN(apply)(Function.unlift(unapply))
}
