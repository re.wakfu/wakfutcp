package wakfutcp.protocol.messages.client

import wakfutcp.protocol.{ClientMessage, Codec}

final case class UserChannelContentMessage(
  channelName: String,
  content: String
) extends ClientMessage {
  override val id = 3151
  override val arch = 4
}

object UserChannelContentMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[UserChannelContentMessage] =
    (utf8(byte), utf8(byte)).imapN(apply)(Function.unlift(unapply))
}
