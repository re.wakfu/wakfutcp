package wakfutcp.protocol.messages.client

import wakfutcp.protocol.{ClientMessage, Codec}

final case class ExchangeInvitationAnswerMessage(
  exchangeId: Long,
  exchangeInvitationResult: Byte
) extends ClientMessage {
  override val id = 6003
  override val arch = 3
}

object ExchangeInvitationAnswerMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[ExchangeInvitationAnswerMessage] =
    (long, byte).imapN(apply)(Function.unlift(unapply))
}
