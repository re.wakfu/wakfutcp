package wakfutcp.protocol.messages.client

import wakfutcp.protocol.{ClientMessage, Codec}

final case class AuthenticationTokenRequestMessage(
  serverId: Int,
  accountId: Long
) extends ClientMessage {
  override val id = 1211
  override val arch = 8
}

object AuthenticationTokenRequestMessage {
  import cats.syntax.apply._
  import Codec._

  implicit val codec: Codec[AuthenticationTokenRequestMessage] =
    (int, long).imapN(apply)(Function.unlift(unapply))
}
