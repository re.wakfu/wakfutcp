package wakfutcp.protocol.messages.client

import wakfutcp.protocol.common.Path
import wakfutcp.protocol.{ClientMessage, Codec}

final case class ActorPathRequestMessage(
  path: Path
) extends ClientMessage {
  override val id = 4113
  override val arch = 3
}

object ActorPathRequestMessage {
  import Codec._
  import cats.syntax.invariant._

  implicit val codec: Codec[ActorPathRequestMessage] =
    Path.codec.imap(apply)(Function.unlift(unapply))
}
