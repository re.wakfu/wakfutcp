package wakfutcp.protocol.messages.client

import wakfutcp.protocol.{ClientMessage, Codec}

final case class TriggerEventMessage(
  eventId: Int
) extends ClientMessage {
  override val arch = 3
  override val id = 11101
}

object TriggerEventMessage {
  import Codec._
  import cats.syntax.invariant._

  implicit val codec: Codec[TriggerEventMessage] =
    int.imap(apply)(Function.unlift(unapply))
}
