package wakfutcp.protocol.messages.client

import wakfutcp.protocol.{ClientMessage, Codec}

final case class GuildPrivateContentMessage(groupId: Long, content: String)
    extends ClientMessage {
  override val id = 523
  override val arch = 6
}

object GuildPrivateContentMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[GuildPrivateContentMessage] =
    (long, utf8(byte)).imapN(apply)(Function.unlift(unapply))
}
