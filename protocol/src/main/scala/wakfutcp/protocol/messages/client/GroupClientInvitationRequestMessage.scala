package wakfutcp.protocol.messages.client

import java.nio.ByteBuffer

import akka.util.ByteStringBuilder
import wakfutcp.protocol.messages.client.GroupClientInvitationRequestMessage.RequestedPlayer
import wakfutcp.protocol.{ClientMessage, Codec}

final case class GroupClientInvitationRequestMessage(
  groupType: Byte,
  groupId: Long,
  fromPartySearch: Boolean,
  occupationId: Long,
  requestedPlayer: RequestedPlayer
) extends ClientMessage {
  override val id = 501
  override val arch = 2
}

object GroupClientInvitationRequestMessage {
  import Codec._
  import cats.syntax.invariant._
  import cats.syntax.apply._

  implicit val codec: Codec[GroupClientInvitationRequestMessage] =
    (byte, long, bool, long, RequestedPlayer.codec)
      .imapN(apply)(Function.unlift(unapply))

  sealed trait RequestedPlayer

  final case class RequestedPlayerName(name: String) extends RequestedPlayer

  object RequestedPlayer {
    implicit val codec: Codec[RequestedPlayer] = new Codec[RequestedPlayer] {
      private[this] val nameCodec: Codec[RequestedPlayerName] =
        utf8(byte).imap(RequestedPlayerName.apply)(Function.unlift(RequestedPlayerName.unapply))

      private[this] val idCodec: Codec[RequestedPlayerId] =
        long.imap(RequestedPlayerId.apply)(Function.unlift(RequestedPlayerId.unapply))

      override def encode(
        builder: ByteStringBuilder,
        t: RequestedPlayer
      ): ByteStringBuilder = t match {
        case n: RequestedPlayerName =>
          builder.putByte(0)
          nameCodec.encode(builder, n)
        case id: RequestedPlayerId =>
          builder.putByte(1)
          idCodec.encode(builder, id)
      }

      override def decode(bb: ByteBuffer): RequestedPlayer =
        bb.get match {
          case 0 => nameCodec.decode(bb)
          case 1 => idCodec.decode(bb)
        }
    }
  }

  final case class RequestedPlayerId(id: Long) extends RequestedPlayer
}
