package wakfutcp.protocol.messages.client

import wakfutcp.protocol.{ClientMessage, Codec}

final case class GroupInvitationByIdAnswerMessage(
  groupType: Byte,
  invitationAccepted: Boolean,
  inviterId: Long
) extends ClientMessage {
  override val id = 541
  override val arch = 6
}

object GroupInvitationByIdAnswerMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[GroupInvitationByIdAnswerMessage] =
    (byte, bool, long).imapN(apply)(Function.unlift(unapply))
}
