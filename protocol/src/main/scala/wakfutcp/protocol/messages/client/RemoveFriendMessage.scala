package wakfutcp.protocol.messages.client

import wakfutcp.protocol.{ClientMessage, Codec}

final case class RemoveFriendMessage(
  friendName: String
) extends ClientMessage {
  override val id = 3133
  override val arch = 4
}

object RemoveFriendMessage {
  import Codec._
  import cats.syntax.invariant._

  implicit val codec: Codec[RemoveFriendMessage] =
    utf8(byte).imap(apply)(Function.unlift(unapply))
}
