package wakfutcp.protocol.messages.client

import wakfutcp.protocol.{ClientMessage, Codec}

final case class UserPrivateContentMessage(
  user: String,
  content: String
) extends ClientMessage {
  override val id = 3155
  override val arch = 4
}

object UserPrivateContentMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[UserPrivateContentMessage] =
    (utf8(byte), utf8(byte)).imapN(apply)(Function.unlift(unapply))
}
