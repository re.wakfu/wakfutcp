package wakfutcp.protocol.messages.client

import wakfutcp.protocol.{ClientMessage, Codec}

final case class RemoveIgnoreMessage(
  ignoreName: String
) extends ClientMessage {
  override val id = 3135
  override val arch = 4
}

object RemoveIgnoreMessage {
  import Codec._
  import cats.syntax.invariant._

  implicit val codec: Codec[RemoveIgnoreMessage] =
    utf8(byte).imap(apply)(Function.unlift(unapply))
}
