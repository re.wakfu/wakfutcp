package wakfutcp.protocol.messages.client

import wakfutcp.protocol.{ClientMessage, Codec}

final case class EndConnectionMessage() extends ClientMessage {
  override val id = 1
  override val arch = 0
}

object EndConnectionMessage {
  implicit val codec: Codec[EndConnectionMessage] =
    Codec.identity(EndConnectionMessage())
}
