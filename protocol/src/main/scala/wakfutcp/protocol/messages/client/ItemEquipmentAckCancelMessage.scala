package wakfutcp.protocol.messages.client

import wakfutcp.protocol.{ClientMessage, Codec}

final case class ItemEquipmentAckCancelMessage(
  characterId: Long
) extends ClientMessage {
  override val id = 5201
  override val arch = 3
}

object ItemEquipmentAckCancelMessage {
  import Codec._
  import cats.syntax.invariant._

  implicit val codec: Codec[ItemEquipmentAckCancelMessage] =
    long.imap(apply)(Function.unlift(unapply))
}
