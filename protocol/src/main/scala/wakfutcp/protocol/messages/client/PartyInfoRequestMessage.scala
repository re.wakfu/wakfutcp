package wakfutcp.protocol.messages.client

import wakfutcp.protocol.{ClientMessage, Codec}

final case class PartyInfoRequestMessage() extends ClientMessage {
  override val id = 525
  override val arch = 6
}

object PartyInfoRequestMessage {
  implicit val codec: Codec[PartyInfoRequestMessage] =
    Codec.identity(PartyInfoRequestMessage())
}
