package wakfutcp.protocol.messages.client

import wakfutcp.protocol.{ClientMessage, Codec}

final case class ExchangeRemoveItemMessage(
  itemId: Long,
  exchangeId: Long,
  itemQuantity: Short
) extends ClientMessage {
  override val id = 6011
  override val arch = 3
}

object ExchangeRemoveItemMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[ExchangeRemoveItemMessage] =
    (long, long, short).imapN(apply)(Function.unlift(unapply))
}
