package wakfutcp.protocol.messages.client

import wakfutcp.protocol.{ClientMessage, Codec}

final case class Message2086(language: String) extends ClientMessage {
  override val id = 2086
  override val arch = 2
}

object Message2086 {
  import Codec._
  import cats.syntax.invariant._

  implicit val codec: Codec[Message2086] =
    utf8(int).imap(apply)(Function.unlift(unapply))
}
