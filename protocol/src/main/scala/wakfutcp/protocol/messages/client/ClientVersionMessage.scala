package wakfutcp.protocol.messages.client

import wakfutcp.protocol.common.Version
import wakfutcp.protocol.{ClientMessage, Codec}

final case class ClientVersionMessage(
  version: Version.WithBuild
) extends ClientMessage {
  override val id = 7
  override val arch = 0
}

object ClientVersionMessage {
  import Codec._
  import cats.syntax.invariant._

  implicit val codec: Codec[ClientVersionMessage] =
    Version.codecWithBuild.imap(apply)(Function.unlift(unapply))
}
