package wakfutcp.protocol.messages.client

import javax.crypto.Cipher

import akka.util.ByteString
import wakfutcp.protocol.{ClientMessage, Codec, Encoder}

final case class ClientDispatchAuthenticationMessage(
  encryptedCredentials: Array[Byte]
) extends ClientMessage {
  override val id = 1026
  override val arch = 8
}

object ClientDispatchAuthenticationMessage {
  import Codec._
  import cats.syntax.apply._
  import cats.syntax.invariant._

  implicit val codec: Codec[ClientDispatchAuthenticationMessage] =
    byteArray(int).imap(apply)(Function.unlift(unapply))

  def create(
    credentials: CredentialData,
    cipher: Cipher
  ): ClientDispatchAuthenticationMessage = {
    val bs = Encoder[CredentialData]
      .encode(ByteString.newBuilder, credentials)
      .result()

    ClientDispatchAuthenticationMessage(
      cipher.doFinal(bs.toArray)
    )
  }

  final case class CredentialData(salt: Long, login: String, password: String)

  object CredentialData {
    implicit val codec: Codec[CredentialData] =
      (long, utf8(byte), utf8(byte)).imapN(apply)(Function.unlift(unapply))
  }
}
