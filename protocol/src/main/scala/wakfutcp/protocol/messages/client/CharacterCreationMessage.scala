package wakfutcp.protocol.messages.client

import wakfutcp.protocol.{ClientMessage, Codec}

final case class CharacterCreationMessage(
  characterId: Long,
  sex: Byte,
  skinColorIndex: Byte,
  hairColorIndex: Byte,
  pupilColorIndex: Byte,
  skinColorFactor: Byte,
  hairColorFactor: Byte,
  clothIndex: Byte,
  faceIndex: Byte,
  breed: Short,
  name: String
) extends ClientMessage {
  override val arch = 2
  override val id = 2053
}

object CharacterCreationMessage {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[CharacterCreationMessage] =
    (long, byte, byte, byte, byte, byte, byte, byte, byte, short, utf8(byte))
      .imapN(apply)(Function.unlift(unapply))
}
