package wakfutcp.protocol

import java.nio.ByteBuffer

trait Decoder[+T] {
  def decode(bb: ByteBuffer): T
}

object Decoder {
  @inline def apply[T: Decoder]: Decoder[T] =
    implicitly[Decoder[T]]
}
