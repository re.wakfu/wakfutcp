package wakfutcp.protocol

import java.nio.ByteOrder

import akka.util.ByteStringBuilder

trait Encoder[-T] {
  def encode(builder: ByteStringBuilder, t: T): ByteStringBuilder
}

object Encoder {
  implicit val DefaultOrder: ByteOrder = ByteOrder.BIG_ENDIAN

  @inline def apply[T: Encoder]: Encoder[T] =
    implicitly[Encoder[T]]
}
