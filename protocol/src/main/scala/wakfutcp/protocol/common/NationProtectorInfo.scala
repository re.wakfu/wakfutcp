package wakfutcp.protocol.common

import wakfutcp.protocol.Codec

final case class NationProtectorInfo(
  protectorId: Int,
  nationId: Int,
  isChaos: Boolean,
  cash: Int,
  fleaTaxValue: Float,
  marketTaxValue: Float,
  currentSatisfaction: Int,
  totalSatisfaction: Int
)

object NationProtectorInfo {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[NationProtectorInfo] =
    (int, int, bool, int, float, float, int, int)
      .imapN(apply)(Function.unlift(unapply))
}
