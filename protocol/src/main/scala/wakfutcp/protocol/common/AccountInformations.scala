package wakfutcp.protocol.common

import wakfutcp.protocol.{BinarSerialCodec, BinarSerialPart, Codec}

sealed trait AccountInformations extends BinarSerialPart

object AccountInformations {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: BinarSerialCodec[AccountInformations] = {
    case 0 => Public.codec
    case 1 => Private.codec
  }

  final case class Public(
    accountId: Long,
    subscriptionLevel: Int,
    antiAddictionLevel: Int,
    field3: Byte,
    accountExpirationDate: Long,
    field5: Array[Inner0],
    adminRights: Array[Int],
    nickName: String,
    community: Community,
    flags: Array[Flag]
  ) extends AccountInformations {
    override def id: Byte = 0
  }

  final case class Inner0(field0: Int, field1: Long)

  object Inner0 {
    implicit val codec: Codec[Inner0] =
      (int, long).imapN(apply)(Function.unlift(unapply))
  }

  object Public {
    implicit val codec: Codec[Public] =
      (
        long,
        int,
        int,
        byte,
        long,
        array(int, Inner0.codec),
        arrayC(100, int),
        utf8(ubyte),
        Community.codec,
        array(short, Flag.codec))
        .imapN(apply)(Function.unlift(unapply))

  }

  final case class Flag(id: Byte, value: Long)

  object Flag {
    implicit val codec: Codec[Flag] =
      (byte, long).imapN(apply)(Function.unlift(unapply))
  }

  final case class Private() extends AccountInformations {
    override def id: Byte = 1
  }

  object Private {
    implicit val codec: Codec[Private] = ???
  }
}
