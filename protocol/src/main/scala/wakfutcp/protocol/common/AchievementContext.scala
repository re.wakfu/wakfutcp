package wakfutcp.protocol.common

import wakfutcp.protocol.common.AchievementContext._
import wakfutcp.protocol.Codec

final case class AchievementContext(
  version: Int,
  history: Array[History],
  variables: Array[Variable],
  achievements: Array[Achievement],
  objectives: Array[Int],
  followed: Array[Int]
)

object AchievementContext {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[AchievementContext] = (
    int,
    array(ubyte, History.codec),
    array(int, Variable.codec),
    array(int, Achievement.codec),
    array(int, int),
    array(ubyte, int))
    .imapN(apply)(Function.unlift(unapply))

  final case class History(
    achievementId: Int,
    unlockTime: Long
  )

  object History {
    implicit val codec: Codec[History] =
      (int, long).imapN(apply)(Function.unlift(unapply))
  }

  final case class Variable(
    id: Int,
    value: Long
  )

  object Variable {
    implicit val codec: Codec[Variable] =
      (int, long).imapN(apply)(Function.unlift(unapply))
  }

  final case class Achievement(
    id: Int,
    active: Boolean,
    complete: Boolean,
    lastCompleted: Long,
    startTime: Long
  )

  object Achievement {
    implicit val codec: Codec[Achievement] =
      (int, bool, bool, long, long)
        .imapN(apply)(Function.unlift(unapply))
  }
}
