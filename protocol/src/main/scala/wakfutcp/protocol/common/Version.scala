package wakfutcp.protocol.common

import wakfutcp.protocol.Codec

final case class Version(
  major: Byte,
  minor: Short,
  revision: Byte
)

object Version {
  import Codec._
  import cats.syntax.apply._

  final case class WithBuild(
    version: Version,
    build: String
  )

  implicit val codec: Codec[Version] =
    (byte, short, byte).imapN(apply)(Function.unlift(unapply))

  implicit val codecWithBuild: Codec[WithBuild] =
    (codec, utf8(byte))
      .imapN(WithBuild.apply)(Function.unlift(WithBuild.unapply))
}
