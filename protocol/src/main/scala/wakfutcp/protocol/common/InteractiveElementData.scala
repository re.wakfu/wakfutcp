package wakfutcp.protocol.common

import wakfutcp.protocol.{BinarSerialCodec, BinarSerialPart, Codec}

sealed trait InteractiveElementData extends BinarSerialPart

object InteractiveElementData {
  import Codec._
  import cats.syntax.apply._
  import cats.syntax.invariant._

  implicit val codec: BinarSerialCodec[InteractiveElementData] = {
    case 0 => GlobalData.codec
    case 1 => SpecificData.codec
    case 2 => SharedData.codec
    case 3 => Codec.identity(Empty)
  }

  final case class Action(field0: Short, field1: Int)

  object Action {
    implicit val codec: Codec[Action] =
      (short, int).imapN(apply)(Function.unlift(unapply))
  }

  final case class GlobalData(actions: Array[Action]) extends InteractiveElementData {
    override def id: Byte = 0
  }

  object GlobalData {
    implicit val codec: Codec[GlobalData] =
      array(byte, Action.codec).imap(apply)(Function.unlift(unapply))
  }

  final case class SpecificData(
    worldId: Short,
    x: Int,
    y: Int,
    z: Short,
    state: Short,
    visible: Boolean,
    usable: Boolean,
    direction: Byte,
    activationPattern: Short,
    positionTriggers: Array[Position],
    parameters: String,
    properties: Array[Byte],
    templateId: Int
  ) extends InteractiveElementData {
    override def id: Byte = 1
  }

  object SpecificData {
    implicit val codec: Codec[SpecificData] = (
      short,
      int,
      int,
      short,
      short,
      bool,
      bool,
      byte,
      short,
      array(short, Position.codec),
      utf8(ushort),
      byteArray(ubyte),
      int)
      .imapN(apply)(Function.unlift(unapply))
  }

  final case class SharedData(
    state: Short,
    visible: Boolean,
    usable: Boolean,
    blockingMovements: Boolean,
    blockingLineOfSight: Boolean,
    visibleContent: Byte,
    properties: Array[Byte]
  ) extends InteractiveElementData {
    override def id: Byte = 2
  }

  object SharedData {
    implicit val codec: Codec[SharedData] =
      (short, bool, bool, bool, bool, byte, byteArray(int))
        .imapN(apply)(Function.unlift(unapply))
  }

  final case object Empty extends InteractiveElementData {
    override def id: Byte = 4
  }

  // TODO: there's more
}
