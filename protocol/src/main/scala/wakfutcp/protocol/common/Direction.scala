package wakfutcp.protocol.common

import enumeratum.values.{ByteEnum, ByteEnumEntry}
import wakfutcp.protocol.Codec

import scala.collection.immutable

sealed abstract class Direction(val value: Byte, x: Int, y: Int) extends ByteEnumEntry

case object Direction extends ByteEnum[Direction] {
  import Codec._
  import cats.syntax.invariant._

  implicit val codec: Codec[Direction] =
    byte.imap(withValueOpt(_).getOrElse(None))(_.value)

  case object East extends Direction(0, 1, -1)
  case object SouthEast extends Direction(1, 1, 0)
  case object South extends Direction(2, 1, 1)
  case object SouthWest extends Direction(3, 0, 1)
  case object West extends Direction(4, -1, 1)
  case object NorthWest extends Direction(5, -1, 0)
  case object North extends Direction(6, -1, -1)
  case object NorthEast extends Direction(7, 0, -1)
  case object Top extends Direction(8, 0, 0)
  case object Bottom extends Direction(9, 0, 0)
  case object None extends Direction(-1, 0, 0)

  def values: immutable.IndexedSeq[Direction] = findValues
}
