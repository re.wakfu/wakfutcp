package wakfutcp.protocol.common

import wakfutcp.protocol.Codec

final case class Position(
  x: Int,
  y: Int,
  z: Short
)

object Position {
  import Codec._
  import cats.syntax.apply._

  implicit val codec: Codec[Position] =
    (int, int, short).imapN(apply)(Function.unlift(unapply))
}
