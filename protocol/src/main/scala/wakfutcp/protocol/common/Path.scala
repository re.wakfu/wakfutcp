package wakfutcp.protocol.common

import java.nio.ByteBuffer

import akka.util.ByteStringBuilder
import cats.syntax.apply._
import wakfutcp.protocol.Codec

final case class Path(starting: Position, steps: Array[PathStep])

object Path {
  import Codec._
  implicit val codec: Codec[Path] =
    (Position.codec, array(byte, PathStep.codec))
      .imapN(apply)(Function.unlift(unapply))
}

final case class PathStep(direction: Direction, heightDiff: Int)

object PathStep {
  implicit val codec: Codec[PathStep] = new Codec[PathStep] {
    override def encode(
      builder: ByteStringBuilder,
      t: PathStep
    ): ByteStringBuilder = {
      val data = ((t.direction.value & 0x7) << 5) | (t.heightDiff & 0x1F)
      builder.putByte(data.toByte)
    }

    override def decode(bb: ByteBuffer): PathStep = {
      val data = bb.get
      val direction = Direction.withValue(data >> 5 & 0x7 toByte)
      val heightDiff = data & 0x1F
      PathStep(
        direction,
        if ((heightDiff & 0x10) != 0x0) {
          heightDiff | 0xFFFFFFE0
        } else heightDiff
      )
    }
  }
}
