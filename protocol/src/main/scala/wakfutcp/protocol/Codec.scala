package wakfutcp.protocol

import java.nio.ByteBuffer

import akka.util.{ByteString, ByteStringBuilder}
import cats.InvariantMonoidal
import cats.implicits._
import org.joda.time.DateTime

import scala.collection.mutable.ListBuffer
import scala.reflect.ClassTag

/** Defines an entity that is both an encoder and a decoder for the type T.
  *
  * @tparam T type to be decoded/encoded
  */
trait Codec[T] extends Encoder[T] with Decoder[T]

object Codec {

  import Encoder.DefaultOrder
  import wakfutcp.protocol.util.extensions._

  @inline def apply[T: Codec]: Codec[T] =
    implicitly[Codec[T]]

  /** Returns a codec that always returns the specified value and encodes nothing.
    *
    * @param t value to return
    * @tparam T type of the value
    * @return codec that always returns the specified value and encodes nothing
    */
  def identity[T](t: T): Codec[T] = new Codec[T] {
    override def encode(builder: ByteStringBuilder, v: T): ByteStringBuilder =
      builder

    override def decode(bb: ByteBuffer): T = t
  }

  /** A codec for a single signed byte.
    */
  val byte: Codec[Byte] = new Codec[Byte] {
    override def encode(builder: ByteStringBuilder, t: Byte): ByteStringBuilder =
      builder.putByte(t)

    override def decode(bb: ByteBuffer): Byte = bb.get
  }

  /** A codec for that encodes/decodes booleans as 1 (true) and 0 (false).
    */
  val bool: Codec[Boolean] =
    byte.imap(_ =!= 0)(if (_) 1 else 0)

  /** A codec for a single unsigned byte.
    */
  val ubyte: Codec[Short] = byte.imap(_ & 0xFF toShort)(_ & 0xFF toByte)

  /** A codec for a single signed short (2 bytes).
    */
  val short: Codec[Short] = new Codec[Short] {
    override def encode(builder: ByteStringBuilder, t: Short): ByteStringBuilder =
      builder.putShort(t)

    override def decode(bb: ByteBuffer): Short = bb.getShort
  }

  /** A codec for a single unsigned short (2 bytes).
    */
  val ushort: Codec[Int] = short.imap(_ & 0xFFFF)(_ & 0xFFFF toShort)

  /** A codec for a single signed int (4 bytes).
    */
  val int: Codec[Int] = new Codec[Int] {
    override def encode(builder: ByteStringBuilder, t: Int): ByteStringBuilder =
      builder.putInt(t)

    override def decode(bb: ByteBuffer): Int = bb.getInt
  }

  /** A codec for a single unsigned int (4 bytes).
    */
  val uint: Codec[Long] = int.imap(_ & 0xFFFFFFFFL)(_ & 0xFFFFFFFF toInt)

  /** A codec for a single signed long (8 bytes).
    */
  val long: Codec[Long] = new Codec[Long] {
    override def encode(builder: ByteStringBuilder, t: Long): ByteStringBuilder =
      builder.putLong(t)

    override def decode(bb: ByteBuffer): Long = bb.getLong
  }

  /** A codec for a single IEEE 754 float (4 bytes).
    */
  val float: Codec[Float] = new Codec[Float] {
    override def encode(builder: ByteStringBuilder, t: Float): ByteStringBuilder =
      builder.putFloat(t)

    override def decode(bb: ByteBuffer): Float = bb.getFloat
  }

  /** A codec for a single IEEE 754 double (8 bytes).
    */
  val double: Codec[Double] = new Codec[Double] {
    override def encode(builder: ByteStringBuilder, t: Double): ByteStringBuilder =
      builder.putDouble(t)

    override def decode(bb: ByteBuffer): Double = bb.getDouble
  }

  /** A codec for a date (based on signed long).
    */
  val dateTime: Codec[DateTime] =
    long.imap(new DateTime(_))(_.getMillis)

  /** Returns a codec for a constant length UTF-8 string.
    *
    * @param length constant length of the string
    * @return a codec for a constant length UTF-8 string
    */
  def utf8C(length: Int): Codec[String] =
    byteArrayC(length).imap(new String(_, "UTF-8"))(_.getBytes("UTF-8"))

  /** Returns a codec for a variable length UTF-8 string prefixed with an integral.
    *
    * @param prefix prefix codec
    * @tparam P type of the integral prefix
    * @return a codec for a variable length UTF-8 string
    */
  def utf8[P: Integral](prefix: Codec[P]): Codec[String] =
    byteArray(prefix).imap(new String(_, "UTF-8"))(_.getBytes("UTF-8"))

  /** A codec that reads all remaining bytes.
    */
  def remainingBytes: Codec[Array[Byte]] = new Codec[Array[Byte]] {
    override def encode(builder: ByteStringBuilder, t: Array[Byte]): ByteStringBuilder =
      builder.putBytes(t)

    override def decode(bb: ByteBuffer): Array[Byte] = bb.getRemainingBytes
  }

  /** Returns a codec that reads values into an array until the end of the stream.
    *
    * @param inner array element codec
    * @tparam T type of the array elements
    * @return a codec that reads values into an array until the end of the stream
    */
  def repeatUntilEnd[T: ClassTag](inner: Codec[T]): Codec[Array[T]] =
    new Codec[Array[T]] {
      override def encode(builder: ByteStringBuilder, t: Array[T]): ByteStringBuilder = {
        t.foreach(inner.encode(builder, _))
        builder
      }

      override def decode(bb: ByteBuffer): Array[T] = {
        val buffer: ListBuffer[T] = ListBuffer()
        while (bb.hasRemaining) buffer.append(inner.decode(bb))
        buffer.toArray
      }
    }

  /** Returns a codec that reads a value within a block of variable size prefixed with an integral.
    *
    * @param prefix prefix codec
    * @param inner value codec
    * @tparam P type of the integral prefix
    * @tparam T type of the value
    * @return a codec that reads a value within a block of variable size prefixed with an integral
    */
  def block[P: Integral, T](prefix: Codec[P], inner: Codec[T]): Codec[T] =
    new Codec[T] {
      override def encode(builder: ByteStringBuilder, t: T): ByteStringBuilder = {
        val tmp = ByteString.newBuilder
        inner.encode(tmp, t)
        val bs = tmp.result()
        prefix.encode(
          builder,
          implicitly[Integral[P]].fromInt(bs.length)
        )
        builder ++= bs
      }

      override def decode(bb: ByteBuffer): T = {
        val length = implicitly[Integral[P]].toInt(prefix.decode(bb))
        val slice = bb.slice()
        bb.position(bb.position + length)
        slice.limit(length)
        inner.decode(slice)
      }
    }

  /** Returns a codec for a constant length byte array.
    *
    * @param length constant length of the string
    * @return a codec for a constant length byte array
    */
  def byteArrayC(length: Int): Codec[Array[Byte]] = new Codec[Array[Byte]] {
    override def encode(builder: ByteStringBuilder, t: Array[Byte]): ByteStringBuilder = {
      assert(t.length === length)
      builder.putBytes(t)
    }

    override def decode(bb: ByteBuffer): Array[Byte] =
      bb.getByteArray(length)
  }

  /** Returns a codec for a variable length byte array prefixed with an integral.
    *
    * @param prefix prefix codec
    * @tparam P type of the prefix
    * @return a codec for a variable length byte array prefixed with an integral
    */
  def byteArray[P: Integral](prefix: Codec[P]): Codec[Array[Byte]] =
    new Codec[Array[Byte]] {
      override def encode(builder: ByteStringBuilder, t: Array[Byte]): ByteStringBuilder = {
        prefix.encode(
          builder,
          implicitly[Integral[P]].fromInt(t.length)
        )
        builder.putBytes(t)
      }

      override def decode(bb: ByteBuffer): Array[Byte] = {
        val length = implicitly[Integral[P]].toInt(prefix.decode(bb))
        bb.getByteArray(length)
      }
    }

  /** Returns a codec for a constant length array of values.
    *
    * @param inner array element codec
    * @tparam T type of the array elements
    * @return a codec for a constant length array of values
    */
  def arrayC[T: ClassTag](length: Int, inner: Codec[T]): Codec[Array[T]] = new Codec[Array[T]] {
    override def encode(builder: ByteStringBuilder, t: Array[T]): ByteStringBuilder = {
      assert(t.length === length)
      t.foreach(inner.encode(builder, _))
      builder
    }

    override def decode(bb: ByteBuffer): Array[T] =
      Array.fill(length)(inner.decode(bb))
  }

  /** Returns a codec for a variable length array of values prefixed with an integral.
    *
    * @param prefix prefix codec
    * @param inner array element codec
    * @tparam P type of the prefix
    * @tparam T type of the array elements
    * @return a codec for a variable length array of values prefixed with an integral
    */
  def array[P: Integral, T: ClassTag](prefix: Codec[P], inner: Codec[T]): Codec[Array[T]] =
    new Codec[Array[T]] {
      override def encode(builder: ByteStringBuilder, t: Array[T]): ByteStringBuilder = {
        prefix.encode(
          builder,
          implicitly[Integral[P]].fromInt(t.length)
        )
        t.foreach(inner.encode(builder, _))
        builder
      }

      override def decode(bb: ByteBuffer): Array[T] = {
        val length = implicitly[Integral[P]].toInt(prefix.decode(bb))
        Array.fill(length)(inner.decode(bb))
      }
    }

  /** Returns a codec for a variable length map of keys and values prefixed with an integral.
    *
    * @param prefix prefix codec
    * @param key key codec
    * @param value value codec
    * @tparam P type of the prefix
    * @tparam K type of the keys
    * @tparam V type of the values
    * @return a codec for a variable length map of keys and values prefixed with an integral
    */
  def map[P: Integral, K, V](prefix: Codec[P], key: Codec[K], value: Codec[V]): Codec[Map[K, V]] =
    new Codec[Map[K, V]] {
      override def encode(builder: ByteStringBuilder, t: Map[K, V]): ByteStringBuilder = {
        prefix.encode(
          builder,
          implicitly[Integral[P]].fromInt(t.size)
        )
        t.foreach {
          case (k, v) =>
            key.encode(builder, k)
            value.encode(builder, v)
        }
        builder
      }

      override def decode(bb: ByteBuffer): Map[K, V] = {
        val length = implicitly[Integral[P]].toInt(prefix.decode(bb))
        Seq
          .fill(length) {
            key.decode(bb) -> value.decode(bb)
          }
          .toMap
      }
    }

  /** Returns a codec for a an option type prefixed with a byte of value 1 (Some) or 0 (None).
    *
    * @param inner value codec
    * @tparam T type of the value
    * @return a codec for a an option type prefixed with a byte of value 1 (Some) or 0 (None)
    */
  def option[T](inner: Codec[T]): Codec[Option[T]] = new Codec[Option[T]] {
    override def encode(builder: ByteStringBuilder, opt: Option[T]): ByteStringBuilder = {
      opt match {
        case Some(v) =>
          builder.putByte(1)
          inner.encode(builder, v)
        case None =>
          builder.putByte(0)
      }
    }

    override def decode(bb: ByteBuffer): Option[T] =
      if (bb.get =!= 0) {
        Some(inner.decode(bb))
      } else None
  }

  /** Returns a codec that reads the stream until the end and parses it as a Protobuf message.
    *
    * @param companion companion object of the Protobuf message
    * @tparam T type of the message
    * @return a codec that reads the stream until the end and parses it as a Protobuf message
    */
  def protobuf[T <: scalapb.GeneratedMessage with scalapb.Message[T]](
    companion: scalapb.GeneratedMessageCompanion[T]): Codec[T] =
    new Codec[T] {

      override def encode(builder: ByteStringBuilder, t: T): ByteStringBuilder = {
        t.writeTo(builder.asOutputStream)
        builder
      }

      override def decode(bb: ByteBuffer): T =
        companion.parseFrom(bb.getRemainingBytes)
    }

  implicit def codecInvariantMonoidal: InvariantMonoidal[Codec] =
    new InvariantMonoidal[Codec] {

      override def unit: Codec[Unit] = new Codec[Unit] {
        override def encode(builder: ByteStringBuilder, t: Unit): ByteStringBuilder = builder

        override def decode(bb: ByteBuffer): Unit = ()
      }

      override def product[A, B](fa: Codec[A], fb: Codec[B]): Codec[(A, B)] =
        new Codec[(A, B)] {
          override def encode(builder: ByteStringBuilder, t: (A, B)): ByteStringBuilder = {
            fa.encode(builder, t._1)
            fb.encode(builder, t._2)
            builder
          }

          override def decode(bb: ByteBuffer): (A, B) =
            (fa.decode(bb), fb.decode(bb))
        }

      override def imap[A, B](fa: Codec[A])(f: (A) => B)(g: (B) => A): Codec[B] =
        new Codec[B] {
          override def encode(builder: ByteStringBuilder, t: B): ByteStringBuilder =
            fa.encode(builder, g(t))

          override def decode(bb: ByteBuffer): B =
            f(fa.decode(bb))
        }
    }
}
