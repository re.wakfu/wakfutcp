package wakfutcp.client.util

import java.net.{InetAddress, InetSocketAddress}
import java.security.KeyFactory
import java.security.spec.X509EncodedKeySpec
import javax.crypto.Cipher

import akka.actor.typed.scaladsl._
import akka.actor.typed.{ActorRef, Behavior, Terminated}
import akka.actor.{ActorSystem, PoisonPill}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Tcp
import akka.util.ByteString
import wakfutcp.client.util.Auth.Credentials
import wakfutcp.protocol.ServerMessage
import wakfutcp.protocol.common.{Proxy, Version, WorldInfo}
import wakfutcp.protocol.messages.client.ClientDispatchAuthenticationMessage.CredentialData
import wakfutcp.protocol.messages.client._
import wakfutcp.protocol.messages.server._
import wakfutcp.protocol.util.extensions.client._
import wakfutcp.protocol.util.stream
import wakfutcp.protocol.util.stream.Recv

object GameSession {
  def behavior(
    address: InetSocketAddress,
    credentials: Credentials,
    version: Version,
    onConnect: (ActorRef[GameSession.Message], ActorRef[ByteString]) => Behavior[stream.Message[ServerMessage]])(
    selectWorld: Seq[(Proxy, WorldInfo)] => Proxy)(
    implicit system: ActorSystem,
    materializer: ActorMaterializer
  ): Behavior[Message] = Behaviors.setup { ctx =>
    val authHandler = stream.withConnection(Auth.behavior(ctx.self, _, credentials, version, selectWorld))
    ctx.system.log.debug("Connecting to auth server at {}", address)
    Tcp()
      .outgoingConnection(address)
      .join(stream.client.flow(ctx.spawn(authHandler, "Auth")))
      .run()
    Behaviors.immutable { (ctx, msg) =>
      msg match {
        case ConnectWorld(worldServerAddress, token) =>
          ctx.system.log.debug("Connecting to world server at {}", worldServerAddress)
          val worldHandler =
            stream.withConnection { conn =>
              World.behavior(ctx.self, conn, token, version, onConnect(ctx.self, conn))
            }
          Tcp()
            .outgoingConnection(worldServerAddress)
            .join(stream.client.flow(ctx.spawn(worldHandler, "World")))
            .run()
          Behaviors.same
        case Shutdown =>
          Behaviors.stopped
      }
    }
  }

  sealed trait Message
  final case class ConnectWorld(address: InetSocketAddress, token: String) extends Message
  case object Shutdown extends Message
}

object Auth {
  def behavior(
    session: ActorRef[GameSession.Message],
    connection: ActorRef[ByteString],
    credentials: Credentials,
    version: Version,
    selectWorld: Seq[(Proxy, WorldInfo)] => Proxy
  ): Behavior[stream.Message[ServerMessage]] = Behaviors.immutable { (ctx, msg) =>
    msg match {
      case Recv(ClientIpMessage(bytes)) =>
        ctx.system.log.debug("Received client user IP ({})", InetAddress.getByAddress(bytes))
        connection !! ClientVersionMessage(Version.WithBuild(version, "-1"))
        Behaviors.same
      case Recv(ClientVersionResultMessage(success, required)) =>
        ctx.system.log.debug("Version check successful: {}, required: {}", success, required)
        connection !! ClientPublicKeyRequestMessage(8)
        Behaviors.same
      case Recv(ClientPublicKeyMessage(salt, pubKey)) =>
        ctx.system.log.debug("Received public key with salt {}", salt)
        val cert = new X509EncodedKeySpec(pubKey)
        val keyFactory = KeyFactory.getInstance("RSA")
        val cipher = Cipher.getInstance("RSA")
        cipher.init(Cipher.ENCRYPT_MODE, keyFactory.generatePublic(cert))
        connection !!
          ClientDispatchAuthenticationMessage.create(
            CredentialData(salt, credentials.login, credentials.password), cipher)
        Behaviors.same
      case Recv(ClientDispatchAuthenticationResultMessage(result, _)) =>
        ctx.system.log.debug("Public key authentication result: {}", result)
        connection !! ClientProxiesRequestMessage()
        Behaviors.same
      case Recv(ClientProxiesResultMessage(proxies, worlds)) =>
        ctx.system.log.debug("Received client proxies list")
        val choice = selectWorld(proxies.zip(worlds))
        connection !! AuthenticationTokenRequestMessage(choice.id, 0)
        authenticateWorld(session, connection, choice)
      case other =>
        ctx.system.log.debug("Unhandled auth server message: {}", other)
        Behaviors.same
    }
  }

  private def authenticateWorld(
    session: ActorRef[GameSession.Message],
    connection: ActorRef[ByteString],
    proxy: Proxy
  ): Behavior[stream.Message[ServerMessage]] = Behaviors.immutable[stream.Message[ServerMessage]] { (ctx, msg) =>
    msg match {
      case Recv(AuthenticationTokenResultMessage.Success(token)) =>
        ctx.system.log.debug("Received authentication token: {}", token)
        val address = new InetSocketAddress(proxy.server.address, proxy.server.ports.head)
        session ! GameSession.ConnectWorld(address, token)
        connection.upcast[Any] ! PoisonPill
        Behaviors.same
      case Recv(AuthenticationTokenResultMessage.Failure) =>
        ctx.system.log.error("Token authentication failed")
        Behaviors.stopped
      case other =>
        ctx.system.log.debug("Unhandled auth server message: {}", other)
        Behaviors.same
    }
  }.onSignal {
    case (_, Terminated(`connection`)) =>
      Behaviors.stopped
  }

  final case class Credentials(login: String, password: String)
}

object World {
  def behavior(
    session: ActorRef[GameSession.Message],
    connection: ActorRef[ByteString],
    token: String,
    version: Version,
    onConnect: Behavior[stream.Message[ServerMessage]]
  ): Behavior[stream.Message[ServerMessage]] = Behaviors.immutable { (ctx, msg) =>
    msg match {
      case Recv(ClientIpMessage(bytes)) =>
        ctx.system.log.debug("Received client IP from game server: {}", InetAddress.getByAddress(bytes))
        connection !! ClientVersionMessage(Version.WithBuild(version, "-1"))
        Behaviors.same
      case Recv(ClientVersionResultMessage(success, required)) =>
        ctx.system.log.debug("Version check successful: {}, required: {}", success, required)
        connection !! ClientAuthenticationTokenMessage(token)
        Behaviors.same
      case Recv(result: ClientAuthenticationResultsMessage) =>
        ctx.system.log.debug("Client authentication result: {}", result)
        Behaviors.same
      case Recv(result: WorldSelectionResultMessage) =>
        ctx.system.log.debug("World selection result: {}", result)
        onConnect
      case other =>
        ctx.system.log.debug("Unhandled message during world auth: {}", other)
        Behaviors.same
    }
  }
}
