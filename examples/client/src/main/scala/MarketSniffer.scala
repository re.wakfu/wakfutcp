import akka.actor.typed.scaladsl._
import akka.actor.typed.{ActorRef, Behavior}
import akka.util.ByteString
import wakfutcp.client.util.GameSession
import wakfutcp.protocol.ServerMessage
import wakfutcp.protocol.common.MarketEntry
import wakfutcp.protocol.messages.client._
import wakfutcp.protocol.messages.server._
import wakfutcp.protocol.util.extensions.client._
import wakfutcp.protocol.util.stream
import wakfutcp.protocol.util.stream.Recv

object MarketSniffer {
  def behavior(
    session: ActorRef[GameSession.Message],
    connection: ActorRef[ByteString]
  ): Behavior[stream.Message[ServerMessage]] = Behaviors.immutable { (ctx, msg) =>
    msg match {
      case Recv(CharactersListMessage(characters)) =>
        import wakfutcp.protocol.raw.CharacterData._

        val Some(char) =
          characters.collectFirst {
            case c if c.select[Name].name == "Derp" => c
          }
        connection !! CharacterSelectionMessage(char.select[Id].id, char.select[Name].name)
        Behaviors.same
      case Recv(CharacterEnterPartitionMessage(_, _)) =>
        ctx.system.log.debug("Entered a partition")
        connection !! InteractiveElementActionMessage(20849, 12)
        Behaviors.same
      case Recv(MarketConsultResultMessage(sales, count)) =>
        ctx.system.log.debug("Initial market interaction successful, {} entries", count)
        connection !!
          MarketConsultRequestMessage(
            -1,
            -1,
            -1,
            -1,
            10.toShort,
            lowestMode = false,
            Array.empty
          )
        receiveAndAsk(session, connection, sales.toList, sales.length)
      case other =>
        ctx.system.log.debug("Other message: {}", other)
        Behaviors.same
    }
  }

  def receiveAndAsk(
    session: ActorRef[GameSession.Message],
    connection: ActorRef[ByteString],
    entries: List[MarketEntry],
    start: Int
  ): Behavior[stream.Message[ServerMessage]] = Behaviors.immutable { (ctx, msg) =>
    msg match {
      case Recv(MarketConsultResultMessage(sales, count)) =>
        ctx.system.log.debug("Market interaction successful")
        val next = start + 10
        if (next >= count) {
          handleData(entries ::: sales.toList)
          ctx.system.log.debug("Done with the work")
          session ! GameSession.Shutdown
          Behaviors.stopped
        } else {
          connection !!
            MarketConsultRequestMessage(
              -1,
              -1,
              -1,
              -1,
              next.toShort,
              lowestMode = false,
              Array.empty
            )
          receiveAndAsk(session, connection, entries ::: sales.toList, next)
        }
      case _ =>
        Behaviors.same
    }
  }

  def handleData(data: List[MarketEntry]): Unit = {
    // do stuff with data
    println(data.head)
  }
}
