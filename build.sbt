
scalaOrganization in ThisBuild := "org.typelevel"
scalaVersion in ThisBuild := "2.12.4-bin-typelevel-4"

lazy val commonSettings = Seq(
  organization := "wakfutcp",
  version := "0.3.0-SNAPSHOT",
  scalacOptions ++= Seq(
    "-deprecation",
    "-feature",
    "-language:existentials",
    "-language:postfixOps",
    "-language:higherKinds",
    "-language:experimental.macros",
    "-unchecked",
    "-Xlint:-unused,_",
    "-Xfatal-warnings",
    "-Xfuture",
    "-Yinduction-heuristics",
    "-Xstrict-patmat-analysis",
    "-Xlint:strict-unsealed-patmat",
    "-opt:l:inline"
  )
)

lazy val protocol = (project in file("protocol"))
  .settings(commonSettings)
  .settings(
    moduleName := "wakfutcp-protocol",
    resolvers += "Akka Snapshots" at "http://repo.akka.io/snapshots/",
      libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-actor" % "2.5.11",
      "com.typesafe.akka" %% "akka-stream" % "2.5.11",
      "com.typesafe.akka" %% "akka-stream-typed" % "2.5.11",
      "com.typesafe.akka" %% "akka-actor-typed" % "2.5.11",
      "org.typelevel" %% "cats-core" % "1.1.0",
      "com.chuusai" %% "shapeless" % "2.3.3",
      "com.beachape" %% "enumeratum" % "1.5.13",
      "com.github.nscala-time" %% "nscala-time" % "2.18.0"
    ),
    PB.targets in Compile := Seq(
      scalapb.gen() -> (sourceManaged in Compile).value
    )
  )

lazy val clientExample = (project in file("examples/client"))
  .settings(commonSettings)
  .settings(
    moduleName := "wakfutcp-example-client"
  )
  .dependsOn(protocol)

lazy val serverExample = (project in file("examples/server"))
  .settings(commonSettings)
  .settings(
    moduleName := "wakfutcp-example-server"
  )
  .dependsOn(protocol)
